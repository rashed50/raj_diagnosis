<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use DB;

class AccessControlsTableSeeder extends Seeder
{

    public function run()
    {
        $dev = User::where('email', 'admin@gmail.com')->first();

        if (empty($dev)) {

            $data = [
                [
                    'id'=>'1',
                    'name' => 'Hotel Administrator',
                    'email' => 'admin@gmail.com',
                    'password' => Hash::make(1234),
                    'user_type' => 'administrator'
                ]
            ];

            User::insert($data);
        }
    }
}
