<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompanyProfile;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = CompanyProfile::where('comp_id',1)->first();

        if (empty($company)) {
          $data = [
              [
                  'comp_id'=>'1',
                  'comp_name' => 'Raj Diagonsis',
              ]
          ];
          CompanyProfile::insert($data);
        }
    }
}
