<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //data for permissions table
        $data = [

            //For Menu System

            ['name' => 'dashboard', 'group_name' => 'dashboard'],



            //For Role System...
            ['name' => 'role-access', 'group_name' => 'role' ],
            ['name' => 'role-list' , 'group_name' => 'role'],
            ['name' => 'role-create', 'group_name' => 'role'],
            ['name' => 'role-show', 'group_name' => 'role'],
            ['name' => 'role-edit', 'group_name' => 'role'],
            ['name' => 'role-delete', 'group_name' => 'role'],

            //For User System...
            ['name' => 'user-access', 'group_name' => 'user' ],
            ['name' => 'user-list', 'group_name' => 'user' ],
            ['name' => 'user-create', 'group_name' => 'user' ],
            ['name' => 'user-show', 'group_name' => 'user' ],
            ['name' => 'user-edit', 'group_name' => 'user' ],
            ['name' => 'user-delete', 'group_name' => 'user' ],

            //For Slider System...
            ['name' => 'slider-access', 'group_name' => 'slider' ],
            ['name' => 'slider-list', 'group_name' => 'slider' ],
            ['name' => 'slider-create', 'group_name' => 'slider' ],
            ['name' => 'slider-show', 'group_name' => 'slider' ],
            ['name' => 'slider-edit', 'group_name' => 'slider' ],
            ['name' => 'slider-delete', 'group_name' => 'slider' ],

            //For Facility System...
            ['name' => 'facility-access', 'group_name' => 'facility' ],
            ['name' => 'facility-list', 'group_name' => 'facility' ],
            ['name' => 'facility-create', 'group_name' => 'facility' ],
            ['name' => 'facility-show', 'group_name' => 'facility' ],
            ['name' => 'facility-edit', 'group_name' => 'facility' ],
            ['name' => 'facility-delete', 'group_name' => 'facility' ],

            //For Tax System...
            ['name' => 'tax-access', 'group_name' => 'tax' ],
            ['name' => 'tax-list', 'group_name' => 'tax' ],
            ['name' => 'tax-create', 'group_name' => 'tax' ],
            ['name' => 'tax-show', 'group_name' => 'tax' ],
            ['name' => 'tax-edit', 'group_name' => 'tax' ],
            ['name' => 'tax-delete', 'group_name' => 'tax' ],

            //For Tax System...
            ['name' => 'coupon-access', 'group_name' => 'coupon' ],
            ['name' => 'coupon-list', 'group_name' => 'coupon' ],
            ['name' => 'coupon-create', 'group_name' => 'coupon' ],
            ['name' => 'coupon-show', 'group_name' => 'coupon' ],
            ['name' => 'coupon-edit', 'group_name' => 'coupon' ],
            ['name' => 'coupon-delete', 'group_name' => 'coupon' ],

            //For RoomCategory System...
            ['name' => 'roomCategory-access', 'group_name' => 'roomCategory' ],
            ['name' => 'roomCategory-list', 'group_name' => 'roomCategory' ],
            ['name' => 'roomCategory-create', 'group_name' => 'roomCategory' ],
            ['name' => 'roomCategory-show', 'group_name' => 'roomCategory' ],
            ['name' => 'roomCategory-edit', 'group_name' => 'roomCategory' ],
            ['name' => 'roomCategory-delete', 'group_name' => 'roomCategory' ],

            //For Package System...
            ['name' => 'package-access', 'group_name' => 'package' ],
            ['name' => 'package-list', 'group_name' => 'package' ],
            ['name' => 'package-create', 'group_name' => 'package' ],
            ['name' => 'package-show', 'group_name' => 'package' ],
            ['name' => 'package-edit', 'group_name' => 'package' ],
            ['name' => 'package-delete', 'group_name' => 'package' ],

            //For Logo System...
            ['name' => 'logo-show', 'group_name' => 'logo' ],
            ['name' => 'logo-edit', 'group_name' => 'logo' ],

            //For Contact System...
            ['name' => 'contact-show', 'group_name' => 'contact' ],
            ['name' => 'contact-edit', 'group_name' => 'contact' ],



        ];
        Permission::insert($data);








        //Data for role user pivot
        $dev = User::where('email', 'admin@gmail.com')->first();
        $data = [
            ['role_id' => 1, 'model_type' => 'App\Models\User', 'model_id' => $dev->id]
        ];
        \DB::table('model_has_roles')->insert($data);

        //Data for role permission pivot
        $permissions = Permission::all();
        foreach ($permissions as $key => $value) {
            $data = [
                ['permission_id' => $value->id, 'role_id' => 1],
            ];

            \DB::table('role_has_permissions')->insert($data);
        }
    }
}
