<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_infos', function (Blueprint $table) {
            $table->id('PartInfoId');
            $table->string('PatiName');
            $table->string('PatiMobile');
            $table->string('PatiGender');
            $table->string('PatiAge');
            $table->string('PatiPhoto')->nullable();
            $table->text('Address')->nullable();
            $table->unsignedBigInteger('CreateById');
            $table->string('_token')->nullable();
            $table->string('_method')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_infos');
    }
}
