<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTestInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_test_infos', function (Blueprint $table) {
            $table->id('ParTestInfoId');
            $table->string('Name');
            $table->string('Mobile');
            $table->string('Gender');
            $table->string('Age');
            $table->string('Photo')->nullable();
            $table->text('Address')->nullable();

            $table->string('InvoiceNo');
            $table->date('TestingDate');
            $table->date('DeleveryDate')->nullable();
            // $table->unsignedBigInteger('LabId');
            $table->string('LabId')->nullable();
            $table->unsignedBigInteger('RefDrId');
            $table->string('TestStatus')->nullable();
            $table->date('Date');
            $table->float('TotalAmount',11,2)->default(0);
            $table->float('Discount',11,2)->default(0);
            $table->float('NetAmount',11,2)->default(0);
            $table->float('PaidAmount',11,2)->default(0);
            $table->float('DueAmount',11,2)->default(0);
            $table->boolean('Status')->default(1);
            $table->unsignedBigInteger('CreateById');
            $table->string('_token')->nullable();
            $table->string('_method')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_test_infos');
    }
}
