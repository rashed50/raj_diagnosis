<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_infos', function (Blueprint $table) {
            $table->id('DoctInfoId');
            $table->string('DoctName');
            $table->text('SpecDetails')->nullable();
            $table->unsignedBigInteger('SpecListTypeId'); // Relation
            $table->boolean('Status')->default(1);
            $table->unsignedBigInteger('CreateById');
            $table->string('_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_infos');
    }
}
