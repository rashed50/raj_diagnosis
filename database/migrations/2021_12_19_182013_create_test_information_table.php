<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_information', function (Blueprint $table) {
            $table->id('TestInfoId');
            $table->string('TestName');
            $table->float('TestActualCost',11,2);
            $table->float('TestRate',11,2);
            $table->boolean('Status')->default(1);
            $table->unsignedBigInteger('TestTypeId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_information');
    }
}
