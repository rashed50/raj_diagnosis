<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialistInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialist_infos', function (Blueprint $table) {
            $table->id('SpListInfoId');
            $table->string('SpecInfoName');
            $table->text('SpecDetails')->nullable();
            $table->boolean('Status')->default(1);
            $table->unsignedBigInteger('SpecListTypeId');
            $table->string('_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialist_infos');
    }
}
