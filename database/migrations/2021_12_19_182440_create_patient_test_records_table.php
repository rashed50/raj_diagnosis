<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTestRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_test_records', function (Blueprint $table) {
            $table->id('PartTestRecoId');
            $table->unsignedBigInteger('ParTestInfoId');
            $table->unsignedBigInteger('TestTypeName');
            $table->unsignedBigInteger('TestId');
            $table->float('Amount',11,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_test_records');
    }
}
