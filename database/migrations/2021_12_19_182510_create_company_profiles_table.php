<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profiles', function (Blueprint $table) {
            $table->id('comp_id');
            $table->string('comp_name',100)->nullable();
            $table->text('comp_address')->nullable();
            $table->string('comp_email1',50)->nullable();
            $table->string('comp_email2',50)->nullable();
            $table->string('comp_phone1',20)->nullable();
            $table->string('comp_phone2',20)->nullable();
            $table->string('comp_mobile1',20)->nullable();
            $table->string('comp_mobile2',20)->nullable();
            $table->string('comp_support_number',20)->nullable();
            $table->string('comp_hotline_number',20)->nullable();
            $table->longText('comp_description')->nullable();
            $table->longText('comp_mission')->nullable();
            $table->longText('comp_vission')->nullable();
            $table->string('comp_contact_address')->nullable();
            $table->string('_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profiles');
    }
}
