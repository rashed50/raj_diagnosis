<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Fontend\FontendController;
use App\Http\Controllers\Admin\MasterController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\TestTypeController;
use App\Http\Controllers\Admin\SpecialistInfoController;
use App\Http\Controllers\Admin\DoctorInfoController;
use App\Http\Controllers\Admin\TestInformationController;
use App\Http\Controllers\Admin\PatientInfoController;
use App\Http\Controllers\Admin\PatientTestController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\DiagonosisCenterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FontendController::class, 'index']);
Route::get('/about', [FontendController::class, 'about']);
Route::get('/contact', [FontendController::class, 'contact']);

/* =============================== Admin Dashboard =============================== */
Route::group(['prefix'=>'admin','middleware' =>['auth:sanctum', 'verified'] ], function(){
    Route::get('dashboard', [MasterController::class, 'index'])->name('admin.dashboard');
    /* +++++++++++++++++ User Route +++++++++++++++++ */
    Route::get('manage/user-list', [UserController::class, 'index'])->name('user.index');
    Route::get('manage/user-profile', [UserController::class, 'profile'])->name('user-profile');
    Route::get('manage/user-create', [UserController::class, 'create'])->name('user.create');
    Route::get('manage/{id}/user-edit', [UserController::class, 'edit'])->name('user.edit');
    Route::get('manage/{id}/user-delete', [UserController::class, 'delete'])->name('user.delete');
    Route::post('manage/user-store', [UserController::class, 'store'])->name('user.store');
    Route::post('manage/{id}/user-update', [UserController::class, 'update'])->name('user.update');
    /* +++++++++++++++++ Role Route +++++++++++++++++ */
    Route::get('role-list', [RoleController::class, 'index'])->name('role.index');
    Route::get('role-create', [RoleController::class, 'create'])->name('role.create');
    Route::get('role-edit/{id}', [RoleController::class, 'edit'])->name('role.edit');
    Route::post('role-store', [RoleController::class, 'store'])->name('role.store');
    Route::post('role-update/{id}', [RoleController::class, 'update'])->name('role.update');
    /* +++++++++++++++++ Test Type Route +++++++++++++++++ */
    Route::get('test_type-list', [TestTypeController::class, 'index'])->name('test_type.index');
    Route::get('test_type-create', [TestTypeController::class, 'create'])->name('test_type.create');
    Route::get('test_type-edit/{id}', [TestTypeController::class, 'edit'])->name('test_type.edit');
    Route::post('test_type-store', [TestTypeController::class, 'store'])->name('test_type.store');
    Route::post('test_type-update/{id}', [TestTypeController::class, 'update'])->name('test_type.update');
    /* +++++++++++++++++ Specialist Route +++++++++++++++++ */
    Route::get('specialist-list', [SpecialistInfoController::class, 'index'])->name('specialist.index');
    Route::get('specialist-create', [SpecialistInfoController::class, 'create'])->name('specialist.create');
    Route::get('specialist-view/{id}', [SpecialistInfoController::class, 'view'])->name('specialist.view');
    Route::get('specialist-edit/{id}', [SpecialistInfoController::class, 'edit'])->name('specialist.edit');
    Route::get('specialist-delete/{id}', [SpecialistInfoController::class, 'delete'])->name('specialist.delete');
    Route::post('specialist-store', [SpecialistInfoController::class, 'store'])->name('specialist.store');
    Route::post('specialist-update/{id}', [SpecialistInfoController::class, 'update'])->name('specialist.update');
    /* +++++++++++++++++ Doctor Route +++++++++++++++++ */
    Route::get('doctor-list', [DoctorInfoController::class, 'index'])->name('doctor.index');
    Route::get('doctor-create', [DoctorInfoController::class, 'create'])->name('doctor.create');
    Route::get('doctor-view/{id}', [DoctorInfoController::class, 'view'])->name('doctor.view');
    Route::get('doctor-edit/{id}', [DoctorInfoController::class, 'edit'])->name('doctor.edit');
    Route::get('doctor-delete/{id}', [DoctorInfoController::class, 'delete'])->name('doctor.delete');
    Route::post('doctor-store', [DoctorInfoController::class, 'store'])->name('doctor.store');
    Route::post('doctor-update/{id}', [DoctorInfoController::class, 'update'])->name('doctor.update');
    /* +++++++++++++++++ Test Inforamtion Route +++++++++++++++++ */
    Route::get('test-list', [TestInformationController::class, 'index'])->name('test.index');
    Route::get('test-create', [TestInformationController::class, 'create'])->name('test.create');
    Route::get('test-view/{id}', [TestInformationController::class, 'view'])->name('test.view');
    // Test type wise test name
    Route::get('test-type/wise/test-name/{testType}', [TestInformationController::class, 'testTypeWiseTestName']);
    // Test type wise test name
    Route::get('test-edit/{id}', [TestInformationController::class, 'edit'])->name('test.edit');
    Route::get('test-delete/{id}', [TestInformationController::class, 'delete'])->name('test.delete');
    Route::post('test-store', [TestInformationController::class, 'store'])->name('test.store');
    Route::post('test-update/{id}', [TestInformationController::class, 'update'])->name('test.update');
    /* +++++++++++++++++ Patient Route +++++++++++++++++ */
    // Route::resource('patient', PatientInfoController::class);
    /* +++++++++++++++++ Patient Test Route +++++++++++++++++ */
    Route::get('patient/test-list', [PatientTestController::class, 'index'])->name('patientTest.index');
    Route::get('patient/test-create', [PatientTestController::class, 'create'])->name('patientTest.create');
    Route::post('patient/test-store', [PatientTestController::class, 'store'])->name('patientTest.store');

    Route::get('patient/test-view/{id}', [PatientTestController::class, 'view'])->name('patientTest.view');
    Route::get('patient/test-edit/{id}', [PatientTestController::class, 'edit'])->name('patientTest.edit');
    Route::get('patient/test-delete/{id}', [PatientTestController::class, 'delete'])->name('patientTest.delete');
    Route::post('patient/test-store', [PatientTestController::class, 'store'])->name('patientTest.store');
    Route::post('patient/test-update/{id}', [PatientTestController::class, 'update'])->name('patientTest.update');
    // add test in cart with ajax
    Route::post('test-info/store-in-cart', [PatientTestController::class, 'storeInCart'])->name('store-in.cart');
    Route::get('test-info/view-in-cart', [PatientTestController::class, 'viewInCart'])->name('view-in.cart');
    Route::post('test-info/delete-in-cart', [PatientTestController::class, 'deleteInCart'])->name('delete-in.cart');
    // add test in cart with ajax
    /* +++++++++++++++++ Report Route +++++++++++++++++ */
    Route::get('report1/create', [ReportController::class, 'report1'])->name('report1.create');
    Route::post('report1/process', [ReportController::class, 'report1Process'])->name('report1.process');

    Route::get('report2/create', [ReportController::class, 'report2'])->name('report2.create');
    Route::post('report2/process', [ReportController::class, 'report2Process'])->name('report2.process');

    /* +++++++++++++++++ Company Route +++++++++++++++++ */
    Route::get('company-profile', [CompanyController::class, 'create'])->name('company.profile');
    Route::post('company-profile/update', [CompanyController::class, 'update'])->name('company.update');
    /* +++++++++++++++++ Diagonisis Center +++++++++++++++++ */
    Route::get('diagonisiscenter/list', [DiagonosisCenterController::class, 'index'])->name('diagonisiscenter.index');

    Route::get('diagonisiscenter-create', [DiagonosisCenterController::class, 'create'])->name('diagonisiscenter.create');

    Route::get('diagonisiscenter-edit/{id}', [DiagonosisCenterController::class, 'edit'])->name('diagonisiscenter.edit');
    Route::post('diagonisiscenter-store', [DiagonosisCenterController::class, 'store'])->name('diagonisiscenter.store');
    Route::post('diagonisiscenter-update/{id}', [DiagonosisCenterController::class, 'update'])->name('diagonisiscenter.update');
    /* +++++++++++++++++ Diagonisis Route +++++++++++++++++ */
    /* +++++++++++++++++ User Route +++++++++++++++++ */
});


/* =============================== Admin Dashboard =============================== */
Route::middleware(['auth:sanctum', 'verified'])->get('admin/dashboard', function () {
    return view('backend.index');
})->name('admin.dashboard');


// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
