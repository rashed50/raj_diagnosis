<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTestRecord extends Model
{
    use HasFactory;
    protected $primaryKey = 'PartTestRecoId';
    protected $guarded = [];
    public $timestamps = false;

    public function testType(){
      return $this->belongsTo('App\Models\TestType');
    }

    public function testName(){
      return $this->belongsTo('App\Models\TestInformation','TestId','TestInfoId');
    }
}
