<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorInfo extends Model
{
    use HasFactory;
    protected $primaryKey = 'DoctInfoId';
    protected $guarded = [];

    public function specialistType(){
      return $this->belongsTo('App\Models\SpecialistType','SpecListTypeId','SpecListTypeId');
    }
}
