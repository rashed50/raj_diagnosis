<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialistType extends Model
{
    use HasFactory;
    protected $primaryKey = 'SpecListTypeId';
    protected $guarded = [];
    public $timestamps = false;
}
