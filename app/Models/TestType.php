<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestType extends Model{
    use HasFactory;
    protected $primaryKey = 'TestTypeId';
    protected $guarded = [];
    public $timestamps = false;
}
