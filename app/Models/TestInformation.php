<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestInformation extends Model
{
    use HasFactory;
    protected $primaryKey = 'TestInfoId';
    protected $guarded = [];

    public function TestType(){
      return $this->belongsTo('App\Models\TestType','TestTypeId','TestTypeId');
    }
}
