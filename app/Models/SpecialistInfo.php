<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialistInfo extends Model
{
    use HasFactory;
    protected $primaryKey = 'SpListInfoId';
    protected $guarded = [];

    public function specialistType(){
      return $this->belongsTo('App\Models\SpecialistType','SpecListTypeId','SpecListTypeId');
    }

}
