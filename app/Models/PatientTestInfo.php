<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTestInfo extends Model
{
    use HasFactory;
    protected $primaryKey = 'ParTestInfoId';
    protected $guarded = [];

    public function testRecord(){
      return $this->hasMany('App\Models\PatientTestRecord','ParTestInfoId','ParTestInfoId');
    }

    public function doctor(){
      return $this->belongsTo('App\Models\DoctorInfo','RefDrId','DoctInfoId');
    }

    public function lab(){
      return $this->belongsTo('App\Models\DiagonosisCenter','LabId','id');
    }

}
