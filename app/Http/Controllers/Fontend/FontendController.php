<?php

namespace App\Http\Controllers\Fontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FontendController extends Controller{
    public function index(){
      return view('fontend.index');
    }

    public function about(){
      return view('fontend.about');
    }

    public function contact(){
      return view('fontend.contact');
    }
}
