<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DiagonosisCenter;
use Session;
use Auth;

class DiagonosisCenterController extends Controller{
  /*
  |--------------------------------------------------------------------------
  | DATABASE OPERATION
  |--------------------------------------------------------------------------
  */

  public function getAll(){
    return $all = DiagonosisCenter::get();
  }

  public function findData($id){
    return $data = DiagonosisCenter::where('id',$id)->first();
  }

  public function store(Request $request){
    $request->validate([
        'name' => 'required',
        'code' => 'required',
        'email' => 'required',
        'phone1' => 'required',
    ]);



    $data = $request->all();
    $data['createBy'] = Auth::user()->id;
    DiagonosisCenter::create($data);
    // insert and redirect
    Session::flash('success','value');
    return Redirect()->back();
    // insert and redirect
  }

  public function update(Request $request,$id){
    $request->validate([
        'name' => 'required',
        'code' => 'required',
        'email' => 'required',
        'phone1' => 'required',

    ]);

    $data = $request->all();
    $data['createBy'] = Auth::user()->id;
    DiagonosisCenter::where('id',$id)->update($data);
    // insert and redirect
    Session::flash('success_update','value');
    return Redirect()->back();
    // insert and redirect
  }






  /*
  |--------------------------------------------------------------------------
  | BLADE OPERATION
  |--------------------------------------------------------------------------
  */

  public function index(){
    $all = $this->getAll();
    return view('backend.diagnoscenter.index',compact('all'));
  }

  public function create(){
    return view('backend.diagnoscenter.add');
  }

  public function edit($id){
    $data = $this->findData($id);
    return view('backend.diagnoscenter.edit',compact('data'));
  }












  /*
  |--------------------------------------------------------------------------
  | API OPERATION
  |--------------------------------------------------------------------------
  */
}
