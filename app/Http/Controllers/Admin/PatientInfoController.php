<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PatientInfo;
use Carbon\Carbon;
use Session;
use Image;
use Auth;

class PatientInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = PatientInfo::get();
        return view('backend.patient.index',compact('all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.patient.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Validation
      $request->validate([
          'PatiName' => 'required',
          'PatiMobile' => 'required',
          'PatiGender' => 'required',
          'PatiAge' => 'required',
      ]);
      // insert data in database
      if($request->file('PatiPhoto')){
        /* making user image */
        $image = $request->file('PatiPhoto');
        $imageName = 'patient-image'.'-'.time().'-'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('uploads/patient/'.$imageName);
        $save_url = 'uploads/patient/'.$imageName;
        // insert data in database
        $data = $request->all();
        $data['CreateById'] = Auth::user()->id;
        $data['PatiPhoto'] = $save_url;
        PatientInfo::create($data);
      }else{
        // insert data in database
        $data = $request->all();
        $data['CreateById'] = Auth::user()->id;
        PatientInfo::create($data);
      }
      // insert and redirect
      Session::flash('success','value');
      return Redirect()->back();
      // insert and redirect

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PatientInfo::where('PartInfoId',$id)->first();
        return view('backend.patient.view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PatientInfo::where('PartInfoId',$id)->first();
        return view('backend.patient.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // Validation
      $request->validate([
          'PatiName' => 'required',
          'PatiMobile' => 'required',
          'PatiGender' => 'required',
          'PatiAge' => 'required',
      ]);
      // insert data in database
      if($request->file('PatiPhoto')){
        if($request->oldImage !=""){
          unlink($request->oldImage);
        }
        /* making user image */
        $image = $request->file('PatiPhoto');
        $imageName = 'patient-image'.'-'.time().'-'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('uploads/patient/'.$imageName);
        $save_url = 'uploads/patient/'.$imageName;
        // insert data in database
        PatientInfo::where('PartInfoId',$id)->update([
          'PatiName' => $request->PatiName,
          'PatiMobile' => $request->PatiMobile,
          'PatiGender' => $request->PatiGender,
          'PatiAge' => $request->PatiAge,
          'PatiPhoto' => $save_url,
          'Address' => $request->Address,
          'CreateById' => Auth::user()->id,
          'updated_at' => Carbon::now(),
        ]);
      }else{
        // insert data in database
        PatientInfo::where('PartInfoId',$id)->update([
          'PatiName' => $request->PatiName,
          'PatiMobile' => $request->PatiMobile,
          'PatiGender' => $request->PatiGender,
          'PatiAge' => $request->PatiAge,
          'Address' => $request->Address,
          'CreateById' => Auth::user()->id,
          'updated_at' => Carbon::now(),
        ]);
      }
      // insert and redirect
      Session::flash('success','value');
      return Redirect()->back();
      // insert and redirect
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      PatientInfo::where('PartInfoId',$id)->delete();
      // insert and redirect
      Session::flash('success_delete','value');
      return Redirect()->back();
      // insert and redirect
    }
}
