<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\DiagonosisCenterController;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Image;
use Session;
use Carbon\Carbon;
use Auth;

class UserController extends Controller{
    /*
    |--------------------------------------------------------------------------
    | DATABASE OPERATION
    |--------------------------------------------------------------------------
    */
    public function getAll(){
      return $all = User::where('id','!=',Auth::user()->id)->get();
    }

    public function findData($id){
      return $data = User::where('id',$id)->first();
    }

    public function delete($id){
      $delete = User::where('id',$id)->delete();
      // insert and redirect
      if($delete){
        Session::flash('success_delete','value');
        return Redirect()->back();
      }else{
        Session::flash('error','value');
        return Redirect()->back();
      }
      // insert and redirect
    }

    public function store(Request $request){
      // validation
      $this->validate($request,[
        'password' => 'required|min:8|confirmed',
        'email' => 'required|unique:users',
      ],[

      ]);


      if($request->file('image')){
        /* making user image */
        $image = $request->file('image');
        $imageName = 'user-image'.'-'.time().'-'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('uploads/user/'.$imageName);
        $save_url = 'uploads/user/'.$imageName;
        // insert data in database
        $pass = Hash::make($request->password);
        $insert = User::insert([
          'name' => $request->name,
          'email' => $request->email,
          'password' => $pass,
          'user_type' => $request->user_type,
          'LabId' => $request->LabId,
          'profile_photo_path' => $save_url,
          'created_at' => Carbon::now(),
        ]);

        // insert and redirect
        if($insert){
          Session::flash('success','value');
          return Redirect()->back();
        }else{
          Session::flash('error','value');
          return Redirect()->back();
        }
        // insert and redirect


      }else{
        // insert data in database
        $pass = Hash::make($request->password);
        $insert = User::insert([
          'name' => $request->name,
          'email' => $request->email,
          'password' => $pass,
          'user_type' => $request->user_type,
          'LabId' => $request->LabId,
          'created_at' => Carbon::now(),
        ]);

        // insert and redirect
        if($insert){
          Session::flash('success','value');
          return Redirect()->back();
        }else{
          Session::flash('error','value');
          return Redirect()->back();
        }
        // insert and redirect
      }

    }

    public function update(Request $request,$id){
      // validation
      $this->validate($request,[
        'email' => 'required|unique:users,email,'.$id.',id',
      ],[

      ]);

      if($request->file('image')){
        if($request->oldImage != ""){
          unlink($request->oldImage);
        }
        /* making user image */
        $image = $request->file('image');
        $imageName = 'user-image'.'-'.time().'-'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('uploads/user/'.$imageName);
        $save_url = 'uploads/user/'.$imageName;
        // insert data in database
        $update = User::where('id',$id)->update([
          'name' => $request->name,
          'email' => $request->email,
          'user_type' => $request->user_type,
          'LabId' => $request->LabId,
          'profile_photo_path' => $save_url,
          'updated_at' => Carbon::now(),
        ]);

        // insert and redirect
        if($update){
          Session::flash('success','value');
          return Redirect()->back();
        }else{
          Session::flash('error','value');
          return Redirect()->back();
        }
        // insert and redirect


      }else{
        // insert data in database
        $update = User::where('id',$id)->update([
          'name' => $request->name,
          'email' => $request->email,
          'LabId' => $request->LabId,
          'user_type' => $request->user_type,
          'updated_at' => Carbon::now(),
        ]);

        // insert and redirect
        if($update){
          Session::flash('success','value');
          return Redirect()->back();
        }else{
          Session::flash('error','value');
          return Redirect()->back();
        }
        // insert and redirect
      }

    }





    /*
    |--------------------------------------------------------------------------
    | BLADE OPERATION
    |--------------------------------------------------------------------------
    */
    public function index(){
      $all = $this->getAll();
      return view('backend.user.index',compact('all'));
    }

    public function profile(){
      $user = User::where('id',Auth::user()->id)->first();
      return view('backend.user.profile',compact('user'));
    }

    public function create(){
      $roleOBJ = new RoleController();
      $role = $roleOBJ->getAll();
      // --------------------------------
      $diagonosisCenterOBJ = new DiagonosisCenterController();
      $diagnoscenter = $diagonosisCenterOBJ->getAll();
      return view('backend.user.add',compact('role','diagnoscenter'));
    }

    public function edit($id){
      $roleOBJ = new RoleController();
      $role = $roleOBJ->getAll();
      // --------------------------------
      $diagonosisCenterOBJ = new DiagonosisCenterController();
      $diagnoscenter = $diagonosisCenterOBJ->getAll();
      $data = $this->findData($id);
      return view('backend.user.edit',compact('role','data','diagnoscenter'));
    }



    /*
    |--------------------------------------------------------------------------
    | API OPERATION
    |--------------------------------------------------------------------------
    */


}
