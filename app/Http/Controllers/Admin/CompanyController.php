<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyProfile;
use Session;

class CompanyController extends Controller{
    public function getAll(){
      return $profile = CompanyProfile::where('comp_id',1)->first();
    }

    public function create(){
      $profile = $this->getAll();
      return view('backend.company.index',compact('profile'));
    }

    public function update(Request $request){
      $update = CompanyProfile::where('comp_id',1)->update($request->all());
      Session::flash('success_update','value');
      return Redirect()->back();
    }



}
