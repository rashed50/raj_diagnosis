<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\TestTypeController;
use Illuminate\Http\Request;
use App\Models\TestInformation;
use Carbon\Carbon;
use Session;
use Auth;

class TestInformationController extends Controller{
  /*
  |--------------------------------------------------------------------------
  | DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  // Test Type Wise Test name
  public function testTypeWiseTestName($id){
    $data = TestInformation::where('TestTypeId',$id)->get();
    return json_encode($data);
  }
  // Test Type Wise Test name
  public function getAll(){
    return $all = TestInformation::get();
  }

  public function findData($id){
    return $data = TestInformation::where('TestInfoId',$id)->first();
  }

  public function store(Request $request){
    $request->validate([
        'TestName' => 'required',
        'TestActualCost' => 'required',
        'TestRate' => 'required',
        'TestTypeId' => 'required',
    ]);

    $data = $request->all();
    // $data['CreateById'] = Auth::user()->id;
    TestInformation::create($data);
    // insert and redirect
    Session::flash('success','value');
    return Redirect()->back();
    // insert and redirect
  }

  public function update(Request $request,$id){
    $request->validate([
        'TestName' => 'required',
        'TestActualCost' => 'required',
        'TestRate' => 'required',
        'TestTypeId' => 'required',
    ]);

    $data = $request->all();
    // $data['CreateById'] = Auth::user()->id;
    TestInformation::where('TestInfoId',$id)->update($data);
    // insert and redirect
    Session::flash('success_update','value');
    return Redirect()->back();
    // insert and redirect
  }






  /*
  |--------------------------------------------------------------------------
  | BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function testType(){
    $testTypeOBJ = new TestTypeController();
    return $data = $testTypeOBJ->getAll();
  }

  public function index(){
    $all = $this->getAll();
    return view('backend.test.index',compact('all'));
  }

  public function create(){
    $testType = $this->testType();
    return view('backend.test.add',compact('testType'));
  }

  public function edit($id){
    $data = $this->findData($id);
    $testType = $this->testType();
    return view('backend.test.edit',compact('data','testType'));
  }

  public function view($id){
    $data = $this->findData($id);
    return view('backend.test.view',compact('data'));
  }











  /*
  |--------------------------------------------------------------------------
  | API OPERATION
  |--------------------------------------------------------------------------
  */
}
