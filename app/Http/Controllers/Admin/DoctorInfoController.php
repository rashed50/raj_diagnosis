<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\SpecialistInfoController;
use Illuminate\Http\Request;
use App\Models\DoctorInfo;
use Carbon\Carbon;
use Session;
use Auth;

class DoctorInfoController extends Controller{
  /*
  |--------------------------------------------------------------------------
  | DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  public function getAll(){
    return $all = DoctorInfo::get();
  }

  public function findData($id){
    return $data = DoctorInfo::where('DoctInfoId',$id)->first();
  }

  public function store(Request $request){
    $request->validate([
        'DoctName' => 'required',
        'SpecListTypeId' => 'required'
    ]);

    $data = $request->all();
    $data['CreateById'] = Auth::user()->id;
    DoctorInfo::create($data);
    // insert and redirect
    Session::flash('success','value');
    return Redirect()->back();
    // insert and redirect
  }

  public function update(Request $request,$id){
    $request->validate([
        'DoctName' => 'required',
        'SpecListTypeId' => 'required'
    ]);

    $data = $request->all();
    $data['CreateById'] = Auth::user()->id;
    DoctorInfo::where('DoctInfoId',$id)->update($data);
    // insert and redirect
    Session::flash('success_update','value');
    return Redirect()->back();
    // insert and redirect
  }






  /*
  |--------------------------------------------------------------------------
  | BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function specialist(){
    $specialistObj = new SpecialistInfoController();
    return $data = $specialistObj->getAllSpecialistType();
  }

  public function index(){
    $all = $this->getAll();
    return view('backend.doctor.index',compact('all'));
  }

  public function create(){
    $specialist = $this->specialist();
    return view('backend.doctor.add',compact('specialist'));
  }

  public function edit($id){
    $data = $this->findData($id);
    $specialist = $this->specialist();
    return view('backend.doctor.edit',compact('data','specialist'));
  }

  public function view($id){
    $data = $this->findData($id);
    return view('backend.doctor.view',compact('data'));
  }











  /*
  |--------------------------------------------------------------------------
  | API OPERATION
  |--------------------------------------------------------------------------
  */
}
