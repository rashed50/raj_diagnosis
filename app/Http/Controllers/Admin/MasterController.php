<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class MasterController extends Controller{
    public function index(){
      return view('backend.index');
    }














    public function myWork(){
      Role::create(['name' => 'Supper Admin']);
      $role = User::createRole('Supper Admin');
      // return $role;

      $role->givePermissionTo('dashboard.view');

      $user = request()->user();
      $user->assignRole('Supper Admin');

      // Check Permission
      if($user->hasPermissionTo('dashboard.view') ){
        return 'true';
      }else{
        abort(404, 'error');
      }

      // $permission->assignRole($role);
    }
}
