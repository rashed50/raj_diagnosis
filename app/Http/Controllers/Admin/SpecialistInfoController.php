<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SpecialistInfo;
use App\Models\SpecialistType;
use Carbon\Carbon;
use Session;

class SpecialistInfoController extends Controller{
  /*
  |--------------------------------------------------------------------------
  | DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  public function getAllSpecialistType(){
    return $all = SpecialistType::get();
  }

  public function getAll(){
    return $all = SpecialistInfo::get();
  }

  public function findData($id){
    return $data = SpecialistInfo::where('SpListInfoId',$id)->first();
  }

  public function delete($id){
    $delete = SpecialistInfo::where('SpListInfoId',$id)->delete();
    // insert and redirect
    if($delete){
      Session::flash('success_delete','value');
      return Redirect()->back();
    }else{
      Session::flash('error','value');
      return Redirect()->back();
    }
    // insert and redirect
  }

  public function store(Request $request){
    // validation
    $this->validate($request,[
      'SpecInfoName' => 'required',
      'SpecListTypeId' => 'required',
    ],[

    ]);
    // INSERT IN DATABASE
    $insert = SpecialistInfo::create($request->all());
    // insert and redirect
    if($insert){
      Session::flash('success','value');
      return Redirect()->back();
    }else{
      Session::flash('error','value');
      return Redirect()->back();
    }
    // insert and redirect

  }

  public function update(Request $request,$id){
    // validation
    $this->validate($request,[
      'SpecInfoName' => 'required',
      'SpecListTypeId' => 'required',
    ],[

    ]);

    $update = SpecialistInfo::where('SpListInfoId',$id)->update($request->all());
    // insert and redirect
    if($update){
      Session::flash('success','value');
      return Redirect()->back();
    }else{
      Session::flash('error','value');
      return Redirect()->back();
    }
    // insert and redirect

  }





  /*
  |--------------------------------------------------------------------------
  | BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function index(){
    $all = $this->getAll();
    return view('backend.specialist.index',compact('all'));
  }

  public function create(){
    $specialist = $this->getAllSpecialistType();
    return view('backend.specialist.add',compact('specialist'));
  }

  public function edit($id){
    $specialist = $this->getAllSpecialistType();
    $data = $this->findData($id);
    return view('backend.specialist.edit',compact('data','specialist'));
  }

  public function view($id){
    $data = $this->findData($id);
    return view('backend.specialist.view',compact('data'));
  }










  /*
  |--------------------------------------------------------------------------
  | API OPERATION
  |--------------------------------------------------------------------------
  */







  /* _____________________________________________________________________ */
}
