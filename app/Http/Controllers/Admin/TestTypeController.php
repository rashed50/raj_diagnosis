<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TestType;
use Carbon\Carbon;
use Session;
use Auth;

class TestTypeController extends Controller{
  /*
  |--------------------------------------------------------------------------
  | DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  public function getAll(){
    return $all = TestType::get();
  }

  public function findData($id){
    return $data = TestType::where('TestTypeId',$id)->first();
  }

  public function delete($id){
    $delete = TestType::where('TestTypeId',$id)->delete();
    // insert and redirect
    if($delete){
      Session::flash('success_delete','value');
      return Redirect()->back();
    }else{
      Session::flash('error','value');
      return Redirect()->back();
    }
    // insert and redirect
  }

  public function store(Request $request){
    // validation
    $this->validate($request,[
      'TestTypeName' => 'required|unique:test_types,TestTypeName',
    ],[

    ]);
    // INSERT IN DATABASE
    $insert = TestType::create($request->all());
    // insert and redirect
    if($insert){
      Session::flash('success','value');
      return Redirect()->back();
    }else{
      Session::flash('error','value');
      return Redirect()->back();
    }
    // insert and redirect

  }

  public function update(Request $request,$id){
    // validation
    $this->validate($request,[
      'TestTypeName' => 'required|unique:test_types,TestTypeName,'.$id.',TestTypeId',
    ],[

    ]);

    $update = TestType::where('TestTypeId',$id)->update($request->all());

    // insert and redirect
    if($update){
      Session::flash('success','value');
      return Redirect()->back();
    }else{
      Session::flash('error','value');
      return Redirect()->back();
    }
    // insert and redirect

  }





  /*
  |--------------------------------------------------------------------------
  | BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function index(){
    $all = $this->getAll();
    return view('backend.test_type.index',compact('all'));
  }

  public function create(){
    return view('backend.test_type.add');
  }

  public function edit($id){
    $data = $this->findData($id);
    return view('backend.test_type.edit',compact('data'));
  }










  /*
  |--------------------------------------------------------------------------
  | API OPERATION
  |--------------------------------------------------------------------------
  */







  /* _____________________________________________________________________ */
}
