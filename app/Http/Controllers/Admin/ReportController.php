<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\TestTypeController;
use App\Http\Controllers\Admin\PatientTestController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CompanyProfile;
use App\Models\PatientTestInfo;
use Illuminate\Http\Request;

class ReportController extends Controller{
    public function report1(){
      $testTypeOBJ = new TestTypeController();
      $TestType = $testTypeOBJ->getAll();
      return view('backend.report.report1',compact('TestType'));
    }

    public function report2(){
      $userOBJ = new UserController();
      $user = $userOBJ->getAll();
      return view('backend.report.report2',compact('user'));
    }

    public function report1Process(Request $request){
      $companyOBJ = new CompanyController();
      $company = $companyOBJ->getAll();
      $patientTestOBJ = new PatientTestController();
      $report = $patientTestOBJ->report1($request->testType,$request->startDate,$request->testInfoId,$request->endDate);

      $TotalAmount = PatientTestInfo::whereBetween('patient_test_infos.Date',[$request->startDate,$request->endDate])
                                ->where('patient_test_records.TestTypeName',$request->testType)
                                ->where('patient_test_records.TestId',$request->testInfoId)
                                ->leftjoin('patient_test_records','patient_test_infos.ParTestInfoId','=','patient_test_records.ParTestInfoId')
                                ->sum('patient_test_records.Amount');
      return view('backend.report.reportProcess',compact('report','TotalAmount','company'));
    }

    public function report2Process(Request $request){
      $companyOBJ = new CompanyController();
      $company = $companyOBJ->getAll();
      if($request->operatorId != ""){
        $report = PatientTestInfo::where('LabId',$request->operatorId)->whereBetween('Date',[$request->startDate,$request->endDate])->get();

        $total = PatientTestInfo::where('LabId',$request->operatorId)->whereBetween('Date',[$request->startDate,$request->endDate])->sum('NetAmount');

        $paid = PatientTestInfo::where('LabId',$request->operatorId)->whereBetween('Date',[$request->startDate,$request->endDate])->sum('PaidAmount');

        $due = PatientTestInfo::where('LabId',$request->operatorId)->whereBetween('Date',[$request->startDate,$request->endDate])->sum('DueAmount');

        return view('backend.report.report2Process',compact('report','total','paid','due','company'));
      }else{
        $report = PatientTestInfo::whereBetween('Date',[$request->startDate,$request->endDate])->get();

        $total = PatientTestInfo::whereBetween('Date',[$request->startDate,$request->endDate])->sum('NetAmount');

        $paid = PatientTestInfo::whereBetween('Date',[$request->startDate,$request->endDate])->sum('PaidAmount');

        $due = PatientTestInfo::whereBetween('Date',[$request->startDate,$request->endDate])->sum('DueAmount');

        return view('backend.report.report2Process',compact('report','total','paid','due','company'));
      }

    }




}
