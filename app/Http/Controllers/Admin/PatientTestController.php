<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\DoctorInfoController;
use App\Http\Controllers\Admin\TestTypeController;
use App\Http\Controllers\Admin\DiagonosisCenterController;
use App\Http\Controllers\Admin\CompanyController;
use Illuminate\Http\Request;
use App\Models\PatientTestInfo;
use App\Models\PatientTestRecord;
use App\Models\TestInformation;
use Carbon\Carbon;
use Session;
use Auth;
use Cart;
use Image;

class PatientTestController extends Controller{
  /*
  |--------------------------------------------------------------------------
  | DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  // ------------- Report -------------
  public function report1($testType,$startDate,$testInfoId,$endDate){

   return $report = PatientTestInfo::whereBetween('patient_test_infos.Date',[$startDate,$endDate])
                            ->where('patient_test_records.TestTypeName',$testType)
                            ->where('patient_test_records.TestId',$testInfoId)
                            ->leftjoin('patient_test_records','patient_test_infos.ParTestInfoId','=','patient_test_records.ParTestInfoId')
                            ->leftjoin('test_types','patient_test_records.TestTypeName','=','test_types.TestTypeId')
                            ->leftjoin('test_information','patient_test_records.TestId','=','test_information.TestInfoId')
                            ->get();


  }

  // ------------- store in Cart ----------------
  public function storeInCart(Request $request){
    $findTestData = TestInformation::where('TestInfoId',$request->TestName)->first();
    Cart::add([
      'id' => uniqid(),
      'name' => 'Store Patient Test Record',
      'qty' => 1,
      'price' => $findTestData->TestRate,
      'weight' => 1,
      'options' => [
          'TestType' => $request->TestType,
          'TestName' => $request->TestName,
          'TestNameText' => $request->TestNameText,
          'TestTypeText' => $request->TestTypeText,
      ],
    ]);
    return response()->json(['success' => 'Sucessfully Added Cart in New Test']);

  }
  // ------------- store in Cart ----------------
  // ------------- view in table ----------------
  public function viewInCart(){
    $carts = Cart::content();
    $cartQty = Cart::count();
    $cartTotal = Cart::total();

    return response()->json(array(
        'carts' => $carts,
        'cartQty' => $cartQty,
        'cartTotal' => round($cartTotal),
    ));
  }
  // ------------- view in table ----------------
  // ------------- delete in table ----------------
  public function deleteInCart(Request $request){
    Cart::remove($request->rowId);
    return response()->json(['success' => 'Test Inforamtion Remove In Cart']);
  }
  // ------------- view in table ----------------
  public function getAll(){
    return $all = PatientTestInfo::get();
  }

  public function findData($id){
    return $data = PatientTestInfo::where('DoctInfoId',$id)->first();
  }

  public function store(Request $request){
    // $request->validate([
    //     'DoctName' => 'required',
    //     'SpecListTypeId' => 'required'
    // ]);

  //   dd($request->all());

    $insert = PatientTestInfo::insertGetId([
      'Name' => $request->PatiName,
      'Mobile' => $request->PatiMobile,
      'Gender' => $request->PatiGender,
      'Age' => $request->PatiAge,
      'Address' => $request->Address,
      'InvoiceNo' => $request->InvoiceNo,
      'TestingDate' => $request->TestingDate,
      'LabId' => $request->LabId,
      'RefDrId' => $request->RefDrId,
      'Date' => Carbon::now(),
      'TotalAmount' => $request->TotalAmount,
      'Discount' => $request->Discount,
      'NetAmount' => $request->NetAmount,
      'PaidAmount' => $request->PaidAmount,
      'DueAmount' => $request->DueAmount,
      'CreateById' => Auth::user()->id,
      'created_at' => Carbon::now(),
    ]);
    // Cart
    $cartData = Cart::content();

    foreach($cartData as $data) {
      PatientTestRecord::insert([
        'ParTestInfoId' => $insert,
        'TestTypeName' => $data->options->TestType,
        'TestId' => $data->options->TestName,
        'Amount' => $data->price,
      ]);
    }

    Cart::destroy();
    // Fatch Data
    $patientInfo = PatientTestInfo::where('ParTestInfoId',$insert)->first();
    $patientTestIno = PatientTestRecord::where('ParTestInfoId',$insert)->get();
    $companyOBJ = new CompanyController();
    $company = $companyOBJ->getAll();
    // insert and redirect
    Session::flash('success','value');
    return view('backend.patientTest.report',compact('patientInfo','patientTestIno','insert','company'));
    // insert and redirect
  }

  public function update(Request $request,$id){
    $request->validate([
        'DoctName' => 'required',
        'SpecListTypeId' => 'required'
    ]);

    $data = $request->all();
    $data['CreateById'] = Auth::user()->id;
    DoctorInfo::where('DoctInfoId',$id)->update($data);
    // insert and redirect
    Session::flash('success_update','value');
    return Redirect()->back();
    // insert and redirect
  }






  /*
  |--------------------------------------------------------------------------
  | BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function specialist(){
    $specialistObj = new SpecialistInfoController();
    return $data = $specialistObj->getAllSpecialistType();
  }

  public function index(){
    $all = $this->getAll();
    return view('backend.patientTest.index',compact('all'));
  }

  public function create(){
    $invoice = Carbon::now()->format('dmY').uniqid();
    $specialist = $this->specialist();
    // ----------------
    $doctorOBJ = new DoctorInfoController();
    $TestTypeOBJ = new TestTypeController();
    $doctor = $doctorOBJ->getAll();
    $TestType = $TestTypeOBJ->getAll();
    // ----------------
    $DiagonosisCenterOBJ = new DiagonosisCenterController();
    $diagonosis = $DiagonosisCenterOBJ->getAll();
    return view('backend.patientTest.add',compact('specialist','invoice','doctor','TestType','diagonosis'));
  }

  // public function edit($id){
  //   $data = $this->findData($id);
  //   $specialist = $this->specialist();
  //   return view('backend.patientTest.edit',compact('data','specialist'));
  // }
  //
  // public function view($id){
  //   $data = $this->findData($id);
  //   return view('backend.patientTest.view',compact('data'));
  // }











  /*
  |--------------------------------------------------------------------------
  | API OPERATION
  |--------------------------------------------------------------------------
  */
}
