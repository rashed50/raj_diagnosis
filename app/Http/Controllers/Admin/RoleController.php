<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;
use Session;
use Auth;

class RoleController extends Controller{
    /*
    |--------------------------------------------------------------------------
    | DATABASE OPERATION
    |--------------------------------------------------------------------------
    */
    public function getAll(){
      return $all = Role::get();
    }

    public function findData($id){
      return $data = Role::where('id',$id)->first();
    }

    public function store(Request $request){
      $request->validate([
          'name' => 'required|unique:roles,name'
      ]);

      Role::insert([
        'name' => $request->name,
        'guard_name' => 'sanctum',
        'created_at' => Carbon::now(),
      ]);
      // insert and redirect
      Session::flash('success','value');
      return Redirect()->back();
      // insert and redirect
    }

    public function update(Request $request,$id){
      $request->validate([
          'name' => 'required|unique:roles,name,'.$id.',id'
      ]);

      Role::where('id',$id)->update([
        'name' => $request->name,
        'guard_name' => 'sanctum',
        'updated_at' => Carbon::now(),
      ]);
      // insert and redirect
      Session::flash('success_update','value');
      return Redirect()->back();
      // insert and redirect
    }






    /*
    |--------------------------------------------------------------------------
    | BLADE OPERATION
    |--------------------------------------------------------------------------
    */

    public function index(){
      $all = $this->getAll();
      return view('backend.role.index',compact('all'));
    }

    public function create(){
      return view('backend.role.add');
    }

    public function edit($id){
      $data = $this->findData($id);
      return view('backend.role.edit',compact('data'));
    }











    /*
    |--------------------------------------------------------------------------
    | API OPERATION
    |--------------------------------------------------------------------------
    */


}
