    @include('layouts.back_include.header')
    {{-- Sidebar --}}
    @include('layouts.back_include.sidebar')
    {{-- main part --}}
    @yield('content')
    {{-- main part --}}
    @include('layouts.back_include.footer')
