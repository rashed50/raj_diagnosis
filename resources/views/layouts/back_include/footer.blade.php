<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2021<a class="ms-25" href="https://1.envato.market/pixinvent_portfolio" target="_blank">Pixinvent</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<!-- END: Footer-->

   <!-- BEGIN: Vendor JS-->
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/vendors.min.js"></script>
   <!-- BEGIN Vendor JS-->

   <!-- BEGIN: Page Vendor JS-->
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/extensions/moment.min.js"></script>

   {{-- Data Fether Loading... --}}
 <!--   <script>
       $(window).on('load', function() {
           if (feather) {
               feather.replace({
                   width: 14,
                   height: 14
               });
           }
       })
   </script> -->

   <!-- DataTables -->
   <script src="{{ asset('content/backend') }}/custom/datatable/js/jquery.dataTables.min.js"></script>
   <script src="{{ asset('content/backend') }}/custom/datatable/js/dataTables.bootstrap4.min.js"></script>
   <script src="{{ asset('content/backend') }}/custom/datatable/js/dataTables.responsive.min.js"></script>
   <script src="{{ asset('content/backend') }}/custom/datatable/js/responsive.bootstrap4.min.js"></script>

   {{-- Date picker... --}}
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>

   {{-- Multiple slect optino... --}}
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/forms/select/select2.full.min.js"></script>

   <!-- END: Page Vendor JS-->

   <!-- BEGIN: Theme JS-->
   <script src="{{ asset('content/backend') }}/app-assets/js/core/app-menu.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/core/app.js"></script>
   <!-- END: Theme JS-->

   <!-- BEGIN: Page JS-->
   <script src="{{ asset('content/backend') }}/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/pages/page-auth-login.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/forms/pickers/form-pickers.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/forms/form-select2.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/forms/form-validation.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/pages/app-user-edit.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/pages/page-profile.js"></script>
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/pages/page-account-settings.js"></script>
   <!-- END: Page JS-->

     <!-- BEGIN: Page JS-->
   <script src="{{ asset('content/backend') }}/app-assets/js/scripts/pages/modal-edit-user.js"></script>
   <script src="{{ asset('content/backend') }}/sweetalert/sweetalert.min.js"></script>
   <script src="{{ asset('content/backend') }}/sweetalert/code.js"></script>
   <script src="{{ asset('content/backend') }}/custom.js"></script>
    <!-- END: Page JS-->

   {{-- <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script> --}}
   {{-- {!! Toastr::message() !!} --}}

   <script>
       $(document).ready(function() {
           $('#myTable').DataTable();
       });
   </script>
   {{-- ajax script --}}
   <script type="text/javascript">
     $.ajaxSetup({
         headers:{
             'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
         }
     })
     // Add To Test
     function addToTest(){
       var TestType = $('select[id="testType"] option:selected').val();
       var TestName = $('select[id="testInfoId"] option:selected').val();
       var TestNameText = $('select[id="testInfoId"] option:selected').text();
       var TestTypeText = $('select[id="testType"] option:selected').text();

       if(TestType != '' && TestName != ''){
         $.ajax({
             type: "POST",
             dataType: 'json',
             data:{
                 TestType:TestType,
                 TestName:TestName,
                 TestNameText:TestNameText,
                 TestTypeText:TestTypeText
             },
             url: "{{ route('store-in.cart') }}",

             success:function(data){
                 viewToTest();
                 //  start message
                 const Toast = Swal.mixin({
                     toast: true,
                     position: 'top-end',
                     showConfirmButton: false,
                     timer: 3000
                 })

                  if($.isEmptyObject(data.error)){
                      Toast.fire({
                        type: 'success',
                        title: data.success
                      })
                  }else{
                    Toast.fire({
                      type: 'error',
                      title: data.error
                    })
					        }
                  //  end message
             }
         })
         // ajax
       }
       else{
         alert('error');
       }
     }
     // Test in Cart view
     function viewToTest(){
       // ajax
       $.ajax({
            type:'GET',
            url: "{{ route('view-in.cart') }}",
            dataType:'json',
            success:function(response){
                $('input[id="TotalAmount"]').val(response.cartTotal);
                // $('#cartQty').text(response.cartQty);
                var viewCart = ""
                 $.each(response.carts, function(key,value){
                     viewCart += `
                     <tr>
                       <td>${value.options.TestNameText}</td>
                       <td>${value.options.TestTypeText}</td>
                       <td>${value.price}</td>
                       <td>
                         <a id="${value.rowId}" onclick="romoveInTest(this.id)" title="view"><i class="fa fa-trash fa-lg delete_icon"></i></a>
                       </td>
                     </tr>
                     `

                 });
                 $('#cartlistinTest').html(viewCart);
            }
        })
       // ajax
     }
     viewToTest();

     // remove
     function romoveInTest(rowId){
       $.ajax({
            type:'POST',
            url: "{{ route('delete-in.cart') }}",
            data: {rowId:rowId},
            dataType:'json',
            success:function(data){
              viewToTest();
            }
        });
     }
     // remove
   </script>
   @yield('scripts')
</body>
<!-- END: Body-->

</html>
