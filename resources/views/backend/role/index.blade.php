@extends('layouts.backend_master')
@section('title') All Role @endsection
@section('role') active @endsection
@section('all.role') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Role list</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Role list
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success_delete'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Delete Role.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-title">
                          All Role List Here...
                      </h4>
                      <div class="d-flex justify-content-end">
                              <a href="{{ route('role.create') }}" class="btn btn-primary font-weight-bolder ">
                                  <i class="la la-list"></i>Create New Role</a>
                      </div>


                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table id="myTable"
                              class="table table-striped text-center table-bordered dt-responsive nowrap"
                              style="100%">
                              <thead>
                                  <tr>
                                      <th>Sl No</th>
                                      <th>Name</th>
                                      <th>Manage</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach ($all as $item)
                                  <tr style="background-color: #F5F5F5; text-align: center;">
                                      <td>{{ $loop->iteration }}</td>
                                      <td>{{ $item->name }}</td>
                                      <td>
                                        {{-- <a href="#" title="view"><i class="fa fa-plus-square fa-lg view_icon"></i></a> --}}
                                        <a href="{{ route('role.edit',$item->id) }}" title="edit"><i class="fas fa-pencil-square fa-lg edit_icon"></i>Edit</a>
                                      </td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
