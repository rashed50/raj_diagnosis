@extends('layouts.backend_master')
@section('title') Update New Test @endsection
@section('test') active @endsection
@section('all.test') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Update Test Create</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Update Test Create
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Update Test.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">
                        New Test Update Here...
                        </h4>
                        <a href="{{ route('test.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('test.update',$data->TestInfoId) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                              <div class="form-group row custom_form_group{{ $errors->has('TestName') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Name:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Test Name" class="form-control" id="TestName" name="TestName" value="{{ $data->TestName }}" required>
                                    @if ($errors->has('TestName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('TestName') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('TestActualCost') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Actual Cost:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Actual Cost" class="form-control" id="TestActualCost" name="TestActualCost" value="{{ $data->TestActualCost }}" required>
                                    @if ($errors->has('TestActualCost'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('TestActualCost') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('TestRate') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Rate:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Test Rate" class="form-control" id="TestRate" name="TestRate" value="{{ $data->TestRate }}" required>
                                    @if ($errors->has('TestRate'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('TestRate') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                               <div class="form-group row custom_form_group{{ $errors->has('TestTypeId') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Type:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <select class="form-control" name="TestTypeId" required>
                                      <option value="">Select Test Type</option>
                                      @foreach ($testType as $test)
                                        <option value="{{ $test->TestTypeId }}" {{ $test->TestTypeId == $data->TestTypeId ? 'selected' : '' }}>{{ $test->TestTypeName }}</option>
                                      @endforeach
                                    </select>

                                    @if ($errors->has('TestTypeId'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('TestTypeId') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mr-2">Update</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
