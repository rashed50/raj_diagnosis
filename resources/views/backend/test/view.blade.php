@extends('layouts.backend_master')
@section('title') View Test Inforamtion @endsection
@section('test') active @endsection
@section('all.test') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Test Inforamtion View</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Test Inforamtion View
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">
                        Test Inforamtion View Here...
                        </h4>
                        <a href="{{ route('test.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form">
                            @csrf
                            <div class="card-body">

                              <div class="form-group row custom_form_group">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Name:') }}</label>
                                  <div class="col-sm-7">
                                    <input type="text" disabled class="form-control" value="{{ $data->TestName }}">
                                  </div>
                               </div>

                               <div class="form-group row custom_form_group">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Actual Cost:') }}</label>
                                  <div class="col-sm-7">
                                    <input type="text" disabled class="form-control" value="{{ $data->TestActualCost }}">
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Rate:') }}</label>
                                  <div class="col-sm-7">
                                    <input type="text" disabled class="form-control" value="{{ $data->TestRate }}">
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Test Type:') }}</label>
                                  <div class="col-sm-7">
                                    <input type="text" disabled class="form-control" value="{{ $data->TestType->TestTypeName }}">
                                  </div>
                               </div>

                            </div>
                            <div class="card-footer">
                                {{-- <button type="submit" class="btn btn-primary mr-2">Update</button> --}}
                                {{-- <button type="reset" class="btn btn-secondary">Cancel</button> --}}
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
