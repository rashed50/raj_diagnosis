@extends('layouts.backend_master')
@section('title') All Diagonisis Center @endsection
@section('diagonisiscenter') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      {{-- <div class="content-wrapper container-xxl p-0"> --}}

<div class="content-wrapper">

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success_delete'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Delete Doctor.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-title">
                          All Diagonisis Center List Here...
                      </h4>
                      <div class="d-flex justify-content-end">
                              <a href="{{ route('diagonisiscenter.create') }}" class="btn btn-primary font-weight-bolder ">
                                  <i class="la la-list"></i>Create New Diagonisis Center</a>
                      </div>


                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table id="myTable"
                              class="table table-striped text-center table-bordered dt-responsive nowrap"
                              style="100%">
                              <thead>
                                  <tr>
                                      <th>Sl No</th>
                                      <th>Name</th>
                                      <th>Code</th>
                                      <th>Phone</th>
                                      <th>Email</th>
                                      <th>Address</th>
                                      <th>Manage</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach ($all as $item)
                                  <tr style="background-color: #F5F5F5; text-align: center;">
                                      <td>{{ $loop->iteration }}</td>
                                      <td>{{ $item->name }}</td>
                                      <td>{{ $item->code }}</td>
                                      <td>{{ $item->phone }}</td>
                                      <td>{{ $item->email }}</td>
                                      <td>{{ Str::limit($item->address,50) }}</td>
                                      <td>
                                        <a href="{{ route('diagonisiscenter.edit',$item->id) }}" title="edit"><i class="fas fa-edit fa-lg edit_icon"></i></a>
                                      </td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
