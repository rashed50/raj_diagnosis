@extends('layouts.backend_master')
@section('title') Add Diagonisis @endsection
@section('diagonisiscenter') active @endsection
@section('styles')
@endsection
@section('content')
  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Added New Diagonisis.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            Create Diagonisis Center Here...
                        </h4>
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('diagonisiscenter.index') }}" class="btn btn-primary font-weight-bolder ">
                            <i class="la la-list"></i>All Diagonisis Center</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('diagonisiscenter.store') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                              <div class="form-group row custom_form_group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Diagonisis Name:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="name" value="{{ old('name') }}" required placeholder="Enter Name">
                                  @if ($errors->has('name'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group{{ $errors->has('code') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Code:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="code" value="{{ old('code') }}" required placeholder="Enter Code">
                                  @if ($errors->has('code'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('code') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Email:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Enter Email">
                                  @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group{{ $errors->has('phone1') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Phone 1:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="phone1" value="{{ old('phone1') }}" required placeholder="Enter Phone 1">
                                  @if ($errors->has('phone1'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('phone1') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group">
                                <label class="col-sm-3 ccontrol-label">Phone 2:</label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="phone2" value="{{ old('phone2') }}" placeholder="Enter Phone 2">
                                </div>
                            </div>

                            <div class="form-group row custom_form_group">
                                <label class="col-sm-3 ccontrol-label">Address:</label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Enter Address">
                                </div>
                            </div>

                            <div class="form-group row custom_form_group">
                                <label class="col-sm-3 ccontrol-label">Discription:</label>
                                <div class="col-sm-7">
                                  <textarea name="description" class="form-control" placeholder="Enter Discription">{{ old('description') }}</textarea>
                                </div>
                            </div>
                          </div>
                          <div class="card-footer text-center">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                          </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
