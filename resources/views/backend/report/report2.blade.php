@extends('layouts.backend_master')
@section('title') Process Report @endsection
@section('report') active @endsection
@section('report1') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Added New Role.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title"> Report Process Here... </h4>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('report2.process') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                              <div class="row">
                                  <div class="col-md-2"></div>
                                  <div class="col-md-8">
                                    <div class="form-group custom_form_group">
                                       <label class="ccontrol-label">{{ __('User Name:') }}<span class="req_star">*</span></label>
                                       <div class="">
                                         <select name="operatorId" class="select2 form-control" id="operatorId">
                                             <option value="">User Name Select</option>
                                             @foreach ($user as $data)
                                               <option value="{{ $data->id }}">{{ $data->name }}</option>
                                             @endforeach
                                         </select>
                                       </div>
                                     </div>
                                  </div>
                                  <div class="col-md-2"></div>
                                  <div class="col-md-6">

                                    <div class="form-group custom_form_group">
                                       <label class="ccontrol-label">{{ __('To Date:') }}<span class="req_star">*</span></label>
                                       <div class="">
                                         <input type="date" name="startDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" class="form-control">
                                       </div>
                                     </div>

                                  </div>

                                  <div class="col-md-6">

                                     <div class="form-group custom_form_group">
                                        <label class="ccontrol-label">{{ __('From Date:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="date" name="endDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" class="form-control">
                                        </div>
                                      </div>

                                  </div>
                              </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary mr-2">Show</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')

@endsection
