<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Pdf Or Print</title>
	<style>
		*{
			margin: 0;
			padding: 0;
			outline: 0;
		}
		ul ol {
			list-style-type:  none;
		}
		a{
			text-decoration: none;
		}
		.main_wrap{
			width: 80%;
			margin: 0 auto;
			padding: 50px;
			background: #f1f1f1;
		}

		.table{
			width: 100%;
		}
		.table, th,td{
			border: 1px solid #333;
  			border-collapse: collapse;
  			text-align: center;
		}

		.table th,td{
			padding: 5px;
		}

		pre {
		  background-color: #dedede;
		  padding: 5px;
		  color: #00ab8e;
		  overflow-x: auto;
		}

		.img-report-file-result {
		  border-top: 1px solid #d5d5d5;
		  margin-top: 3em;
		}
		.header{
			text-align:  center;
			margin-bottom: 10px;
		}
		@media print{
			.print-button{
				display: none;
			}
		}
	</style>
</head>
<body>

	<div class="main_wrap">
		<a style="cursor:pointer" onclick="window.print()" class="print-button">PDF Or Pirnt</a>
		<div class="header">
			<h2>{{ $company->comp_name }}</h2>
			<p> <span>{{ $company->comp_phone1 }}</span> <span>{{ $company->comp_mobile1 }}</span></p>
			<address>{{ $company->comp_contact_address }}</address>
		</div>
		<div class="mdl-grid">
			<div class="mdl-cell--12 col" >
				<div id="img-report-summary">
					<h1 style="margin-bottom:10px">Report 2</h1>
					<table class="table mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
						<thead>
							<th class="mdl-data-table__cell--non-numeric">S.N</th>
							<th class="mdl-data-table__cell--non-numeric">Patient Name</th>
							<th class="mdl-data-table__cell--non-numeric">Mobile</th>
							<th class="mdl-data-table__cell--non-numeric">Address</th>
							<th class="mdl-data-table__cell--non-numeric">Date</th>
							<th class="mdl-data-table__cell--non-numeric">Total</th>
							<th class="mdl-data-table__cell--non-numeric">Paid</th>
							<th class="mdl-data-table__cell--non-numeric">Due</th>
						</tr>
					</thead>
					<tbody>
            @forelse($report as $data)
						<tr>
							<td class="mdl-data-table__cell--non-numeric">{{ $loop->iteration }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->Name }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->Mobile }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->Address }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->Date }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->NetAmount }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->PaidAmount }}</td>
							<td class="mdl-data-table__cell--non-numeric">{{ $data->DueAmount }}</td>
						</tr>
            @empty
              <p style="color:red">Data Not Found!</p>
            @endforelse
						<tr>
							<td colspan="5"><strong style="display: block; margin-right: 30px; text-align:right">Total:</strong></td>
							<td>{{ $total }}</td>
							<td>{{ $paid }}</td>
							<td>{{ $due }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		</div>
	</div>
</body>
</html>
