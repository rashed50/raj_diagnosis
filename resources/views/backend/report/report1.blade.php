@extends('layouts.backend_master')
@section('title') Process Report @endsection
@section('report') active @endsection
@section('report1') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Added New Role.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title"> Report Process Here... </h4>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('report1.process') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                              <div class="row">
                                  <div class="col-md-6">

                                    <div class="form-group custom_form_group">
                                       <label class="ccontrol-label">{{ __('Test Type:') }}<span class="req_star">*</span></label>
                                       <div class="">
                                         <select name="testType" class="select2 form-control" id="testType" required>
                                             <option value="">Test Type Select</option>
                                             @foreach ($TestType as $aTestType)
                                               <option value="{{ $aTestType->TestTypeId }}">{{ $aTestType->TestTypeName }}</option>
                                             @endforeach
                                         </select>
                                         <div class="valid-feedback">Looks good!</div>
                                         <div class="invalid-feedback">Please select Doctor</div>
                                       </div>
                                     </div>

                                    <div class="form-group custom_form_group">
                                       <label class="ccontrol-label">{{ __('To Date:') }}<span class="req_star">*</span></label>
                                       <div class="">
                                         <input type="date" name="startDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" class="form-control">
                                       </div>
                                     </div>

                                  </div>

                                  <div class="col-md-6">
                                    <div class="form-group custom_form_group">
                                       <label class="ccontrol-label">{{ __('Test Name:') }}<span class="req_star">*</span></label>
                                       <div class="">
                                         <select name="testInfoId" class="select2 form-control" id="testInfoId" required>
                                             <option value="">Test Select</option>
                                         </select>
                                         <div class="valid-feedback">Looks good!</div>
                                         <div class="invalid-feedback">Please select Doctor</div>
                                       </div>
                                     </div>

                                     <div class="form-group custom_form_group">
                                        <label class="ccontrol-label">{{ __('From Date:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="date" name="endDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" class="form-control">
                                        </div>
                                      </div>

                                  </div>
                              </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary mr-2">Show</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
  {{-- Partial Script path... --}}
  <script type="text/javascript">
      $(document).ready(function(){
        // test type wise test name
        $('select[name="testType"]').on('change', function(){
            var testType = $(this).val();
            if(testType) {
                $.ajax({
                    url: "{{  url('/admin/test-type/wise/test-name') }}/"+testType,
                    type:"GET",
                    dataType:"json",
                    success:function(result) {
                      if(result == ""){
                        // Division
                        $('select[name="testInfoId"]').empty();
                        $('select[name="testInfoId"]').append('<option value="">Data Not Found! </option>');
                        // District
                      }else{
                        // Division
                        $('select[name="testInfoId"]').empty();
                        $('select[name="testInfoId"]').append('<option value="">Test Select</option>');

                        // Division List
                        $.each(result, function(key, value){
                            $('select[name="testInfoId"]').append('<option value="'+ value.TestInfoId +'">' + value.TestName + '</option>');
                        });
                      }

                    },

                });
            } else{

            }
        });
        // test type wise test name
      });

  </script>

@endsection
