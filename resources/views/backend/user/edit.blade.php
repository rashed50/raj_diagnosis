@extends('layouts.backend_master')
@section('title') Create User @endsection
@section('user') active @endsection
@section('all.user') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">User Update</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">User Update
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Update New User.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">
                        User Update Here...
                        </h4>
                        <a href="{{ route('user.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('user.update',$data->id) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                              <input type="hidden" name="oldImage" value="{{ $data->profile_photo_path }}">
                              <div class="form-group row custom_form_group{{ $errors->has('name') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Name:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Name" class="form-control" id="name" name="name" value="{{ $data->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('email') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Email:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="email" placeholder="Enter Email" class="form-control" id="email" name="email" value="{{ $data->email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                               <div class="form-group row custom_form_group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Role:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <select name="user_type" class="select2 form-control" id="user_type" required="">
                                        <option value="">Role Select</option>
                                        @foreach ($role as $aRole)
                                          <option value="{{ $aRole->id }}" {{ $aRole->id == $data->user_type ? 'selected' : ''}}>{{ $aRole->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback">Looks good!</div>
                                    <div class="invalid-feedback">Please select your Role</div>
                                    @if ($errors->has('user_type'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user_type') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                                </div>

                                <div class="form-group row custom_form_group{{ $errors->has('LabId') ? ' has-error' : '' }}">
                                   <label class="col-sm-3 ccontrol-label">{{ __('Lab Id:') }}<span class="req_star">*</span></label>
                                   <div class="col-sm-7">

                                     <select name="LabId" class="select2 form-control" id="LabId" required="" >
                                         <option value="">Lab Select</option>
                                         @foreach ($diagnoscenter as $item)
                                           <option value="{{ $item->id }}" {{ $item->id == $data->LabId ? 'selected' : '' }}>{{ $item->name }}</option>
                                         @endforeach
                                     </select>

                                     @if($errors->has('LabId'))
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $errors->first('LabId') }}</strong>
                                         </span>
                                     @endif

                                   </div>
                                 </div>

                                <div class="form-group row custom_form_group{{ $errors->has('image') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Image:') }}<span class="req_star">*</span></label>

                                  <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file btnu_browse">
                                                Browse… <input type="file" name="image" id="imgInp3" accept="image/x-png,image/gif,image/jpeg">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                  </div>
                                  <div class="col-md-1">
                                    <img id='img-upload3' width="100"/>
                                  </div>
                                  <div class="col-md-2">
                                    <img src="{{ asset($data->profile_photo_path) }}" width="100" style="margin-left: 30px"/>
                                  </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mr-2">Update</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
