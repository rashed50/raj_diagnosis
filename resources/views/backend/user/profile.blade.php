@extends('layouts.backend_master')
@section('title') User Profile @endsection
@section('user') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">User Profile</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">User Profile
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Added New User.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">
                        User Create Here...
                        </h4>
                        <a href="{{ route('user.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        {{-- Profile Column --}}
                        <div class="col-md-4">
                          <div class="" style="display:flex; justify-content:flex-end">
                          @if($user->profile_photo_path != NULL)
                            <img src=" {{ asset($user->profile_photo_path) }}" alt="" style="width: 150px; height: 150px; border-radius: 50%; text-align:right">
                          @else
                            <img src="{{ asset('uploads') }}/avatar.jpg" alt="" style="width: 150px; height: 150px; border-radius: 50%; text-align:right">
                          @endif
                          </div>
                        </div>
                        {{-- Update Setting --}}
                        <div class="col-md-8">
                          <form id="jquery-val-form">
                              <div class="card-body">
                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-2 ccontrol-label">{{ __('Name:') }}<span class="req_star">*</span></label>
                                    <div class="col-sm-7">
                                      <input type="text" disabled class="form-control" value="{{ $user->name }}">
                                    </div>
                                 </div>

                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-2 ccontrol-label">{{ __('Email:') }}<span class="req_star">*</span></label>
                                    <div class="col-sm-7">
                                      <input type="text" disabled class="form-control" value="{{ $user->email }}">
                                    </div>
                                 </div>

                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-2 ccontrol-label">{{ __('Role:') }}<span class="req_star">*</span></label>
                                    <div class="col-sm-7">
                                      <input type="text" disabled class="form-control" value="{{ $user->role->name }}">
                                    </div>
                                 </div>

                              </div>
                          </form>
                          <!--end::Form-->
                        </div>
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
