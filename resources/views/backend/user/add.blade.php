@extends('layouts.backend_master')
@section('title') Create User @endsection
@section('user') active @endsection
@section('add.user') active @endsection
@section('styles')
  
@endsection

@section('content')
<script src="{{ asset('content/backend') }}/form-validation/parsley.min.js"></script>

  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">User Create</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">User Create
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="" id="">
      {{-- tooltip-validation  tooltip-validations--}}
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Added New User.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title"> User Create Here... </h4>
                        <a href="{{ route('user.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                        <form  action="{{ route('user.store') }}" method="post"
                            enctype="multipart/form-data" id="registerUser" data-parsley-validate>
                            @csrf
                            <div class="card-body">

                              <div class="form-group row custom_form_group{{ $errors->has('name') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Name:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Name" class="form-control" id="name" name="name" value="{{old('name')}}" required data-parsley-pattern="[a-zA-Z ]+$" data-parsley-trigger="keyup" >
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('email') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Email:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="email" placeholder="Enter Email" class="form-control" id="email" name="email" value="{{old('email')}}" required  data-parsley-type="email" data-parsley-trigger="keyup">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('password') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Password:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="password" class="form-control" placeholder="********" id="password" name="password" autocomplete="new-password" data-parsley-length="[8,15]" required data-parsley-trigger="keyup">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Confirmed Password:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="password" class="form-control" placeholder="********" id="password_confirmation" name="password_confirmation" required data-parsley-equalto="#password" data-parsley-trigger="keyup">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                               <div class="form-group row custom_form_group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Role:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <select name="user_type" class="select2 form-control" id="user_type" required data-parsley-trigger="keyup">
                                        <option value="">Role Select</option>
                                        @foreach ($role as $aRole)
                                          <option value="{{ $aRole->id }}">{{ $aRole->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback">Looks good!</div>
                                    <div class="invalid-feedback">Please select your Role</div>
                                    @if ($errors->has('user_type'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user_type') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                                </div>

                               <div class="form-group row custom_form_group{{ $errors->has('LabId') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Lab Name:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">

                                    <select name="LabId" class="select2 form-control" id="LabId" required data-parsley-trigger="keyup" >
                                        <option value="">Lab Select</option>
                                        @foreach ($diagnoscenter as $data)
                                          <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('LabId'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('LabId') }}</strong>
                                        </span>
                                    @endif

                                  </div>
                                </div>

                                <div class="form-group row custom_form_group{{ $errors->has('image') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Image:') }}<span class="req_star">*</span></label>

                                  <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file btnu_browse">
                                                Browse… <input type="file" name="image" id="imgInp3" accept="image/x-png,image/gif,image/jpeg">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <img id='img-upload3' width="200"/>
                                  </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
{{-- javascript --}}
<script type="text/javascript">
  $(function(){
    $("#registerUser").parsley();
  });
</script>
{{-- javascript --}}
@endsection

@section('scripts')
{{-- Partial Script path... --}}


@endsection
