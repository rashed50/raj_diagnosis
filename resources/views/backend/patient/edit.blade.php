@extends('layouts.backend_master')
@section('title') Update Patient @endsection
@section('patient') active @endsection
@section('all.patient') active @endsection
@section('styles')
@endsection
@section('content')

  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Patient Update</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Patient Update
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Update Patient.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">
                        Patient Update Here...
                        </h4>
                        <a href="{{ route('patient.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('patient.update',$data->PartInfoId) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="card-body">
                              <input type="hidden" name="oldImage" value="{{ $data->PatiPhoto }}">
                              <div class="form-group row custom_form_group{{ $errors->has('PatiName') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Patient Name:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Patient Name" class="form-control" id="PatiName" name="PatiName" value="{{ $data->PatiName }}" required>
                                    @if ($errors->has('PatiName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('PatiName') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('PatiMobile') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Patient Mobile:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Patient Mobile" class="form-control" id="PatiMobile" name="PatiMobile" value="{{ $data->PatiMobile }}" required>
                                    @if ($errors->has('PatiMobile'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('PatiMobile') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('PatiAge') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Patient Age:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Patient Age" class="form-control" id="PatiAge" name="PatiAge" value="{{ $data->PatiAge }}" required>
                                    @if ($errors->has('PatiAge'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('PatiAge') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('PatiGender') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Patient Gender:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <select class="form-control" name="PatiGender" required>
                                      <option value="Female" {{ $data->PatiGender == 'Female' ? 'selected' : '' }} >Female</option>
                                      <option value="Male" {{ $data->PatiGender == 'Male' ? 'selected' : '' }} >Male</option>
                                      <option value="Others" {{ $data->PatiGender == 'Others' ? 'selected' : '' }} >Others</option>
                                      <option value="Custom" {{ $data->PatiGender == 'Custom' ? 'selected' : '' }} >Custom</option>
                                    </select>
                                    @if ($errors->has('PatiGender'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('PatiGender') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>


                              <div class="form-group row custom_form_group{{ $errors->has('Address') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Address:') }}<span class="req_star"></span></label>
                                  <div class="col-sm-7">
                                    <textarea name="Address" class="form-control" rows="8" cols="80" placeholder="Details Here ...">{{ $data->Address }}</textarea>
                                    @if ($errors->has('Address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('Address') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                               <div class="form-group row custom_form_group{{ $errors->has('PatiPhoto') ? ' has-error' : '' }}">
                                 <label class="col-sm-3 ccontrol-label">{{ __('Image:') }}<span class="req_star">*</span></label>

                                 <div class="col-sm-4">
                                   <div class="input-group">
                                       <span class="input-group-btn">
                                           <span class="btn btn-default btn-file btnu_browse">
                                               Browse… <input type="file" name="PatiPhoto" id="imgInp3" accept="image/x-png,image/gif,image/jpeg">
                                           </span>
                                       </span>
                                       <input type="text" class="form-control" readonly>
                                   </div>
                                 </div>
                                 <div class="col-md-1">
                                   <img src="{{ asset($data->PatiPhoto) }}" width="100" alt="No Image"/>
                                 </div>
                                 <div class="col-md-2">
                                   <img id='img-upload3' width="100" height="100" style="margin-left: 20px"/>
                                 </div>
                               </div>

                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary mr-2">Update</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
