@extends('layouts.backend_master')
@section('title') Update Company @endsection
@section('company') active @endsection
@section('styles')
@endsection
@section('content')
  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success_update'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Update Company.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title"> Update Company... </h4>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('company.update') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                              <div class="form-group row custom_form_group{{ $errors->has('comp_name_en') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Company Name:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_name" value="{{ $profile->comp_name }} " required>
                                  @if ($errors->has('comp_name_en'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_name_en') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group{{ $errors->has('comp_email1') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Email 1:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="email" class="form-control" name="comp_email1" value="{{ $profile->comp_email1 }}" required>
                                  @if ($errors->has('comp_email1'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_email1') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group{{ $errors->has('comp_email2') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Email 2:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="email" class="form-control" name="comp_email2" value="{{ $profile->comp_email2 }}">
                                  @if ($errors->has('comp_email2'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_email2') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            <div class="form-group row custom_form_group{{ $errors->has('comp_phone1') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Phone 1:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_phone1" value="{{ $profile->comp_phone1 }}" required>
                                  @if ($errors->has('comp_phone1'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_phone1') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group">
                                <label class="col-sm-3 ccontrol-label">Phone 2:</label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_phone2" value="{{ $profile->comp_phone2 }}">

                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_mobile1') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Mobile 1:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_mobile1" value="{{ $profile->comp_mobile1 }}" required>
                                  @if ($errors->has('comp_mobile1'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_mobile1') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_mobile2') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Mobile 2:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_mobile2" value="{{ $profile->comp_mobile2 }}">
                                  @if ($errors->has('comp_mobile2'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_mobile2') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_support_number') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Contact Number:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_support_number" value="{{ $profile->comp_support_number }}">
                                  @if ($errors->has('comp_support_number'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_support_number') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_contact_address') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Contact Address:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <textarea name="comp_contact_address" class="form-control">{{ $profile->comp_contact_address }}</textarea>
                                  @if ($errors->has('comp_contact_address'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_contact_address') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>


                            <div class="form-group row custom_form_group{{ $errors->has('comp_hotline_number') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Hotline Number:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" name="comp_hotline_number" value="{{ $profile->comp_hotline_number }}">
                                  @if ($errors->has('comp_hotline_number'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_hotline_number') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_address') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Address:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <textarea name="comp_address" class="form-control">{{ $profile->comp_address }}</textarea>
                                  @if ($errors->has('comp_address'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_address') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_description') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Description:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <textarea name="comp_description" class="form-control">{{ $profile->comp_description }}</textarea>
                                  @if ($errors->has('comp_description'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_description') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_mission') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Mission:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <textarea name="comp_mission" class="form-control">{{ $profile->comp_mission }}</textarea>
                                  @if ($errors->has('comp_mission'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_mission') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="form-group row custom_form_group{{ $errors->has('comp_vission') ? ' has-error' : '' }}">
                                <label class="col-sm-3 ccontrol-label">Vision:<span class="req_star">*</span></label>
                                <div class="col-sm-7">
                                  <textarea name="comp_vission" class="form-control">{{ $profile->comp_vission }}</textarea>
                                  @if ($errors->has('comp_vission'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('comp_vission') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>

                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary mr-2">Update</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
