@extends('layouts.backend_master')
@section('title') Create Patient @endsection
@section('patient') active @endsection
@section('add.patient') active @endsection
@section('styles')
  <style media="screen">
    hr{
      margin: 0;
    }
  </style>
@endsection
@section('content')

  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

<div class="content-wrapper">

  <div class="content-body">
      <!-- Tooltip validations start -->
      <section class="tooltip-validations" id="tooltip-validation">
          {{-- Response Masseage --}}
          <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                @if(Session::has('success'))
                  <div class="alert alert-success alertsuccess" role="alert">
                     <strong>Successfully!</strong> Added New Patient.
                  </div>
                @endif
                @if(Session::has('error'))
                  <div class="alert alert-warning alerterror" role="alert">
                     <strong>Opps!</strong> please try again.
                  </div>
                @endif
              </div>
              <div class="col-md-2"></div>
          </div>
          {{-- Response Masseage --}}
          <div class="row">
              <div class="col-12">
                  <div class="card">

                      <div class="card-header" style="padding-bottom:0">
                          <h4 class="card-title">
                          Patient Create Here...
                          </h4>
                          <a href="{{ route('patientTest.index') }}"
                              class="btn btn-danger font-weight-bolder float-right mb-0">
                          <i class="la la-list"></i>Back To Record</a>
                      </div>
                      <div class="card-body" style="padding-top:0">
                          <form id="jquery-val-form" action="{{ route('patientTest.store') }}" method="post"
                              enctype="multipart/form-data">
                              @csrf
                              <div class="card-body">

                                <div class="row">
                                  {{-- first Part --}}
                                  <div class="col-md-9">
                                    {{-- first row --}}
                                    <div class="row">
                                      {{-- item --}}
                                      <div class="col-md-3">
                                        {{-- element --}}
                                        <div class="form-group custom_form_group{{ $errors->has('InvoiceNo') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Invoice No:') }}<span class="req_star">*</span></label>
                                            <div class="">
                                              <input type="text" placeholder="Enter Invoice No" class="form-control" id="InvoiceNo" name="InvoiceNo" value="{{ $invoice }}" required>
                                              @if ($errors->has('InvoiceNo'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('InvoiceNo') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                         </div>

                                         <div class="form-group custom_form_group{{ $errors->has('PatiAge') ? ' has-error' : '' }}">
                                             <label class="ccontrol-label">{{ __('Patient Age:') }}<span class="req_star">*</span></label>
                                             <div class="">
                                               <input type="text" placeholder="Enter Patient Age" class="form-control" id="PatiAge" name="PatiAge" value="{{old('PatiAge')}}" required>
                                               @if ($errors->has('PatiAge'))
                                                   <span class="invalid-feedback" role="alert">
                                                       <strong>{{ $errors->first('PatiAge') }}</strong>
                                                   </span>
                                               @endif
                                             </div>
                                          </div>

                                          <div class="form-group custom_form_group{{ $errors->has('LabId') ? ' has-error' : '' }}">
                                             <label class="ccontrol-label">{{ __('Lab Name:') }}<span class="req_star">*</span></label>
                                             <div class="">
                                               <select name="LabId" class="select2 form-control" id="LabId" required="">
                                                   <option value="">Lab Name Select</option>
                                                   @foreach ($diagonosis as $data)
                                                     <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                   @endforeach
                                               </select>
                                               @if ($errors->has('LabId'))
                                                   <span class="invalid-feedback" role="alert">
                                                       <strong>{{ $errors->first('LabId') }}</strong>
                                                   </span>
                                               @endif
                                             </div>
                                           </div>

                                           {{-- image --}}
                                          {{-- <div class="form-group custom_form_group{{ $errors->has('PatiPhoto') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Image:') }}<span class="req_star">(Optional)</span></label>
                                            <div class="row">
                                              <div class="col-sm-9">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-default btn-file btnu_browse">
                                                            Browse… <input type="file" name="PatiPhoto" id="imgInp3" accept="image/x-png,image/gif,image/jpeg">
                                                        </span>
                                                    </span>
                                                    <input type="text" class="form-control" readonly>
                                                </div>
                                              </div>
                                              <div class="col-md-3">
                                                <img id='img-upload3' width="50"/>
                                              </div>
                                            </div>

                                          </div> --}}
                                          {{-- image --}}

                                         {{-- element --}}
                                      </div>
                                      {{-- item --}}
                                      <div class="col-md-3">
                                        {{-- item --}}
                                        <div class="form-group custom_form_group{{ $errors->has('PatiName') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Patient Name:') }}<span class="req_star">*</span></label>
                                            <div class="">
                                              <input type="text" placeholder="Enter Patient Name" class="form-control" id="PatiName" name="PatiName" value="{{old('PatiName')}}" required>
                                              @if ($errors->has('PatiName'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('PatiName') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                         </div>

                                        <div class="form-group custom_form_group{{ $errors->has('Address') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Address:') }}<span class="req_star"></span></label>
                                            <div class="">
                                              <input name="Address" class="form-control" placeholder="Details Here ..." value="{{ old('Address') }}">
                                              @if ($errors->has('Address'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('Address') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                         </div>
                                         {{-- item --}}
                                      </div>
                                      {{-- item --}}
                                      <div class="col-md-3">
                                        {{-- item --}}
                                        <div class="form-group custom_form_group{{ $errors->has('PatiMobile') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Patient Mobile:') }}<span class="req_star">*</span></label>
                                            <div class="">
                                              <input type="text" placeholder="Enter Patient Mobile" class="form-control" id="PatiMobile" name="PatiMobile" value="{{old('PatiMobile')}}" required>
                                              @if ($errors->has('PatiMobile'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('PatiMobile') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                         </div>

                                         <div class="form-group custom_form_group{{ $errors->has('RefDrId') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Reference Doctor:') }}<span class="req_star">*</span></label>
                                            <div class="">
                                              <select name="RefDrId" class="select2 form-control" id="RefDrId" required="">
                                                  <option value="">Reference Doctor Select</option>
                                                  @foreach ($doctor as $aDoctor)
                                                    <option value="{{ $aDoctor->DoctInfoId }}">{{ $aDoctor->DoctName }}</option>
                                                  @endforeach
                                              </select>
                                              @if ($errors->has('RefDrId'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('RefDrId') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                          </div>
                                         {{-- item --}}
                                      </div>
                                      {{-- item --}}
                                      <div class="col-md-3">
                                        {{-- item --}}
                                        <div class="form-group custom_form_group{{ $errors->has('PatiGender') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Patient Gender:') }}<span class="req_star">*</span></label>
                                            <div class="">
                                              <select class="form-control" name="PatiGender" required>
                                                <option value="Female">Female</option>
                                                <option value="Male">Male</option>
                                                <option value="Others">Others</option>
                                                <option value="Custom">Custom</option>
                                              </select>
                                              @if ($errors->has('PatiGender'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('PatiGender') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                         </div>
                                          {{-- item --}}
                                          <div class="form-group custom_form_group{{ $errors->has('TestingDate') ? ' has-error' : '' }}">
                                            <label class="ccontrol-label">{{ __('Testing Date:') }}<span class="req_star">*</span></label>
                                            <div class="">
                                              <input type="date" class="form-control" name="TestingDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                              @if ($errors->has('TestingDate'))
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('TestingDate') }}</strong>
                                                  </span>
                                              @endif
                                            </div>
                                         </div>
                                          {{-- item --}}
                                      </div>
                                    </div>
                                    {{-- second row --}}
                                    <div class="row">
                                      <div class="col-md-12">
                                        <h4 class=""> Add To Patient Test Here... </h4>
                                        <hr>
                                        <div class="row align-items-center">
                                          <div class="col-md-5">
                                            <div class="form-group custom_form_group">
                                               <label class="ccontrol-label">{{ __('Test Type:') }}<span class="req_star">*</span></label>
                                               <div class="">
                                                 <select name="testType" class="select2 form-control" id="testType">
                                                     <option value="">Test Type Select</option>
                                                     @foreach ($TestType as $aTestType)
                                                       <option value="{{ $aTestType->TestTypeId }}">{{ $aTestType->TestTypeName }}</option>
                                                     @endforeach
                                                 </select>
                                                 <div class="valid-feedback">Looks good!</div>
                                                 <div class="invalid-feedback">Please select Doctor</div>
                                               </div>
                                             </div>
                                          </div>

                                          <div class="col-md-5">

                                            <div class="form-group custom_form_group">
                                               <label class="ccontrol-label">{{ __('Test Name:') }}<span class="req_star">*</span></label>
                                               <div class="">
                                                 <select name="testInfoId" class="select2 form-control" id="testInfoId">
                                                     <option value="">Test Select</option>
                                                 </select>
                                                 <div class="valid-feedback">Looks good!</div>
                                                 <div class="invalid-feedback">Please select Doctor</div>
                                               </div>
                                             </div>
                                          </div>
                                          <div class="col-md-2">
                                            <div class="text-right">
                                              <a onclick="addToTest()" style="padding:10px 15px; margin-top:11px" class="btn btn-primary">Add Here</a>
                                            </div>
                                          </div>
                                        </div>
                                        {{-- cart list --}}
                                        <div class="row">
                                          <div class="col-md-12">
                                            <table class="table table-bordered">
                                              <thead>
                                                <tr>
                                                  <th>Test Type</th>
                                                  <th>Test Name</th>
                                                  <th>Test Cost</th>
                                                  <th>Action</th>
                                                </tr>
                                              </thead>
                                              <tbody id="cartlistinTest">

                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                        {{-- cart list --}}
                                      </div>
                                    </div>
                                  </div>
                                  {{-- second Part --}}
                                  <div class="col-md-3">
                                    <div class="form-group custom_form_group{{ $errors->has('TotalAmount') ? ' has-error' : '' }}">
                                        <label class="ccontrol-label">{{ __('Total Amount:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="text" placeholder="Enter Total Amount" class="form-control" id="TotalAmount" name="TotalAmount" value="{{old('TotalAmount')}}" required>
                                          @if ($errors->has('TotalAmount'))
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('TotalAmount') }}</strong>
                                              </span>
                                          @endif
                                        </div>
                                     </div>

                                    <div class="form-group custom_form_group{{ $errors->has('Discount') ? ' has-error' : '' }}">
                                        <label class="ccontrol-label">{{ __('Discount Amount:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="text" placeholder="Enter Discount Amount" class="form-control" id="Discount" name="Discount" value="{{old('Discount')}}" required onkeyup="netAmountCalculation()">
                                          @if ($errors->has('Discount'))
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('Discount') }}</strong>
                                              </span>
                                          @endif
                                        </div>
                                     </div>

                                    <div class="form-group custom_form_group{{ $errors->has('NetAmount') ? ' has-error' : '' }}">
                                        <label class="ccontrol-label">{{ __('Net Amount:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="text" placeholder="Enter Net Amount" class="form-control" id="NetAmount" name="NetAmount" value="{{old('NetAmount')}}" required>
                                          @if ($errors->has('NetAmount'))
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('NetAmount') }}</strong>
                                              </span>
                                          @endif
                                        </div>
                                     </div>

                                     <input type="hidden" id="temporaryField" value="">

                                    <div class="form-group custom_form_group{{ $errors->has('PaidAmount') ? ' has-error' : '' }}">
                                        <label class="ccontrol-label">{{ __('Paid Amount:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="text" placeholder="Enter Paid Amount" class="form-control" id="PaidAmount" name="PaidAmount" value="{{old('PaidAmount')}}" required onkeyup="dueAmountCalculation()">
                                          @if ($errors->has('PaidAmount'))
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('PaidAmount') }}</strong>
                                              </span>
                                          @endif
                                        </div>
                                     </div>

                                    <div class="form-group custom_form_group{{ $errors->has('DueAmount') ? ' has-error' : '' }}">
                                        <label class="ccontrol-label">{{ __('Due Amount:') }}<span class="req_star">*</span></label>
                                        <div class="">
                                          <input type="text" placeholder="Enter Due Amount" class="form-control" id="DueAmount" name="DueAmount" value="{{old('DueAmount')}}" required>
                                          @if ($errors->has('DueAmount'))
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('DueAmount') }}</strong>
                                              </span>
                                          @endif
                                        </div>
                                     </div>
                                  </div>


                                </div>




                              </div>
                              <div class="card-footer text-center">
                                  <button type="submit" class="btn btn-primary mr-2">Submit</button>
                              </div>
                          </form>
                          <!--end::Form-->
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- Tooltip validations end -->
  </div>
  </div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}
<script type="text/javascript">
    $(document).ready(function(){
      // test type wise test name
      $('select[name="testType"]').on('change', function(){
          var testType = $(this).val();
          if(testType) {
              $.ajax({
                  url: "{{  url('/admin/test-type/wise/test-name') }}/"+testType,
                  type:"GET",
                  dataType:"json",
                  success:function(result) {
                    if(result == ""){
                      // Division
                      $('select[name="testInfoId"]').empty();
                      $('select[name="testInfoId"]').append('<option value="">Data Not Found! </option>');
                      // District
                    }else{
                      // Division
                      $('select[name="testInfoId"]').empty();
                      $('select[name="testInfoId"]').append('<option value="">Test Select</option>');

                      // Division List
                      $.each(result, function(key, value){
                          $('select[name="testInfoId"]').append('<option value="'+ value.TestInfoId +'">' + value.TestName + '</option>');
                      });
                    }

                  },

              });
          } else{

          }
      });
      // test type wise test name
    });

</script>
{{-- calculation --}}
<script type="text/javascript">
// Attend Labour Cost
  function netAmountCalculation(){
    var TotalAmount = parseFloat( $('#TotalAmount').val() );
    var Discount = parseFloat( $('#Discount').val() );

    if(Discount >= 0){
        var total_amount = (TotalAmount - Discount);
        $("#NetAmount").val('');
        $("#NetAmount").val(total_amount);
        // temporaryField
        $("#temporaryField").val('');
        $("#temporaryField").val(total_amount);
    }else{
        $("#NetAmount").val('');
        $("#NetAmount").val(TotalAmount);
        //
        // temporaryField
        $("#temporaryField").val('');
        $("#temporaryField").val(TotalAmount);
    }
  }

  //
  function dueAmountCalculation(){
    var TotalAmount = parseFloat( $('#temporaryField').val() );
    var PaidAmount = parseFloat( $('#PaidAmount').val() );

    if(PaidAmount >= 0){
        var total_amount = (TotalAmount - PaidAmount);
        $("#DueAmount").val('');
        $("#DueAmount").val(total_amount);
    }else{
        $("#DueAmount").val('');
        $("#DueAmount").val(TotalAmount);
    }
  }
</script>
@endsection
