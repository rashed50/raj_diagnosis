<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Pdf Or Print</title>
	<style>
		*{
			margin: 0;
			padding: 0;
			outline: 0;
		}
		ul ol {
			list-style-type:  none;
		}
		a{
			text-decoration: none;
		}
		.main_wrap{
			width: 700px;
			margin: 0 auto;
			padding: 10px;
			background: #f1f1f1;
		}
		.topbar{
			display: flex;
			justify-content: space-between;
			align-items: center;
		}
		.topbar_left ul li{
			list-style-type: none;
			margin: 5px 0px;
		}
		.topbar_right ul li,.footbarLeft ul li{
			list-style-type: none;
			margin: 5px 0px;
		}
		/* table */
		.middlebar{
			margin-top: 10px;
		}
		.middlebar .table{
			width: 100%;
		}
		.table, th,td{
			border: 1px solid #333;
  			border-collapse: collapse;
  			text-align: center;
		}

		.table th,td{
			padding: 5px;
		}
		/* footbar */
		.footbarWrap{
			display: flex;
			justify-content: space-between;
			margin-top: 10px;
		}
		.footbarLeft ul li{
			display: flex;
			justify-content: space-between;
		}
		.footbarLeft ul li strong{
			margin-right: 30px;
		}
		.footbarLeft ul li span{
			text-align: right;
			margin-right: 10px;
		}
		/* header */
		.header{
			display: flex;
			justify-content: space-between;
			align-items: center;
			margin-bottom: 10px;
		}
		.header .second{
			text-align: center;
		}
		.header{
			text-align:  center;
			margin-bottom: 10px;
		}
		@media print{
			.print-button{
				display: none;
			}
		}
	</style>
</head>
<body>
	<div class="main_wrap">
		<a style="cursor:pointer" onclick="window.print()" class="print-button">PDF Or Pirnt</a>
		<div class="header">
			<div class="first">
				<a href="#">Logo</a>
			</div>

			<div class="second">
				<h2>{{ $company->comp_name }}</h2>
				<p><span>{{ $company->comp_phone1 }}</span> <span>{{ $company->comp_mobile1 }}</span> </p>
				<address>{{ $company->comp_contact_address }}</address>
			</div>
			<div class="third">
				<a href="#">Logo</a>
			</div>


		</div>
		<!-- first section -->
		<div class="topbar">
			<!-- top Left -->
			<div class="topbar_left">
				<ul>
					<li> <strong>Invoice No:</strong> {{ $patientInfo->InvoiceNo }} </li>
					<li> <strong>Patient Name:</strong> {{ $patientInfo->Name }} </li>
					<li> <strong>Ref Dr:</strong> {{ $patientInfo->doctor->DoctName }} </li>
					<li> <strong>Address:</strong> {{ $patientInfo->Address }} </li>
					<li> <strong>Mobile:</strong> {{ $patientInfo->Mobile }} </li>
				</ul>
			</div>
			<!-- top rigth -->
			<div class="topbar_right">
				<ul>
					<li> <strong>Date:</strong> {{ $patientInfo->Date }} </li>
					<li> <strong>Age:</strong> {{ $patientInfo->Age }} </li>
					<li> <strong>Sex:</strong> {{ $patientInfo->Gender }} </li>
				</ul>
				<div class="labId">
					<p> <strong>LAB Name:</strong> {{ $patientInfo->lab->name }} </p>
				</div>
			</div>
		</div>
		<!-- middlebar -->
		<div class="middlebar">
			<table class="table">
				<thead>
					<tr>
						<th>SNo</th>
						<th>Test Name</th>
						<th>Amount(TK)</th>
					</tr>
				</thead>
				<tbody>
          @foreach($patientTestIno as $data)
            <tr>
  						<td>{{ $loop->iteration }}</td>
  						<td>{{ $data->testName->TestName }}</td>
  						<td>{{ $data->Amount }}</td>
  					</tr>
          @endforeach
				</tbody>
			</table>
		</div>
		<!-- footbar -->
		<div class="footbar">
			<div class="footbarWrap">
				<div class="footbarRight">
					<p>
						<strong>In Word:</strong> One Thousand Two Hundred Tweenty Taka Only
					</p>

					<p style="margin-top: 40px; margin-left: 20px"> <small>Operator:</small> Gidk </p>
				</div>
				<div class="footbarLeft">
					<ul>
						<li> <strong>Total:</strong> <span>{{ $patientInfo->TotalAmount }}</span> </li>
						<li> <strong>Discount:</strong> <span>{{ $patientInfo->Discount }}</span> </li>
						<li> <strong>Net Total:</strong> <span>{{ $patientInfo->NetAmount }}</span> </li>
						<li> <strong>Amount Paid:</strong> <span>{{ $patientInfo->PaidAmount }}</span> </li>
						<li> <strong>Due:</strong> <span>{{ $patientInfo->DueAmount }}</span> </li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end wrap -->
	</div>
</body>
</html>
