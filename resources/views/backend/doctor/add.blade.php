@extends('layouts.backend_master')
@section('title') Create Doctor @endsection
@section('doctor') active @endsection
@section('add.doctor') active @endsection
@section('styles')
@endsection
@section('content')


  <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      {{-- <div class="content-wrapper container-xxl p-0"> --}}
      <div class="content-header row"></div>

      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                  <div class="row breadcrumbs-top">
                      <div class="col-12">
                          <h2 class="content-header-title float-left mb-0">Doctor Create</h2>
                          <div class="breadcrumb-wrapper">
                              <ol class="breadcrumb">
                                  <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a>
                              </li>
                              <li class="breadcrumb-item active">Doctor Create
                              </li>
                          </ol>
                      </div>
                  </div>
              </div>
          </div>
      </div>

<div class="content-body">
    <!-- Tooltip validations start -->
    <section class="tooltip-validations" id="tooltip-validation">
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              @if(Session::has('success'))
                <div class="alert alert-success alertsuccess" role="alert">
                   <strong>Successfully!</strong> Added New Doctor.
                </div>
              @endif
              @if(Session::has('error'))
                <div class="alert alert-warning alerterror" role="alert">
                   <strong>Opps!</strong> please try again.
                </div>
              @endif
            </div>
            <div class="col-md-2"></div>
        </div>
        {{-- Response Masseage --}}
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">
                        Doctor Create Here...
                        </h4>
                        <a href="{{ route('doctor.index') }}"
                            class="btn btn-danger font-weight-bolder float-right mb-0">
                        <i class="la la-list"></i>Back To Record</a>
                    </div>
                    <div class="card-body">
                        <form id="jquery-val-form" action="{{ route('doctor.store') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                              <div class="form-group row custom_form_group{{ $errors->has('DoctName') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Doctor Name:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <input type="text" placeholder="Enter Doctor Name" class="form-control" id="DoctName" name="DoctName" value="{{old('DoctName')}}" required>
                                    @if ($errors->has('DoctName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('DoctName') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                               <div class="form-group row custom_form_group{{ $errors->has('SpecListTypeId') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Type:') }}<span class="req_star">*</span></label>
                                  <div class="col-sm-7">
                                    <select class="form-control" name="SpecListTypeId" required>
                                      <option value="">Select Specialist Type</option>
                                      @foreach ($specialist as $spec)
                                        <option value="{{ $spec->SpecListTypeId }}">{{ $spec->SpecListTypeName }}</option>
                                      @endforeach
                                    </select>

                                    @if ($errors->has('SpecListTypeId'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('SpecListTypeId') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                              <div class="form-group row custom_form_group{{ $errors->has('SpecDetails') ? ' has-error' : '' }}">
                                  <label class="col-sm-3 ccontrol-label">{{ __('Details:') }}<span class="req_star"></span></label>
                                  <div class="col-sm-7">
                                    <textarea name="SpecDetails" class="form-control" rows="8" cols="80" placeholder="Details Here ...">{{ old('SpecDetails') }}</textarea>
                                    @if ($errors->has('SpecDetails'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('SpecDetails') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                               </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tooltip validations end -->
</div>
</div>



</div>
@endsection
@section('scripts')
{{-- Partial Script path... --}}

@endsection
